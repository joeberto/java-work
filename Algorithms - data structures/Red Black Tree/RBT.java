public class RBT<E extends Comparable<E>> {
    private Node<E> root;

    public RBT(){
        root = null;
    }

    public Node<E> getRoot(){
        return root;
    }

    public void insert(E data){
    	 boolean done = false;
         Node<E> temp = root;

         while(!done){
             if (root == null) {
                 root = new Node<E>(data);
                 done = true;
             } else if (temp.getData().compareTo(data) > 0){
                 if (temp.getLeftChild() == null){
                     temp.setLeftChild(new Node<E>(data));
                     temp.getLeftChild().setParent(temp);
                     //where the insert fix up occurs
                     fixInsert(temp.getLeftChild());
                     done = true;
                 }
                 temp = temp.getLeftChild();
             } else if (temp.getData().compareTo(data) <= 0){
                 if (temp.getRightChild() == null){
                     temp.setRightChild(new Node<E>(data));
                     temp.getRightChild().setParent(temp);
                     //where the insert fixup occurs
                     fixInsert(temp.getRightChild());
                     done = true;
                 }
                 temp = temp.getRightChild();
             }
         }
     }


    public Node<E> search(E data){
        // Return the node that corresponds with the given data
        // Note: No need to worry about duplicate values added to the tree
    	boolean done = false;
        Node<E> temp = root;


        while(!done){
            if (temp == null){
                return null;
            }
            if(temp.getData().compareTo(data) == 0){
                done = true;
            } else if (temp.getData().compareTo(data) > 0){
                temp = temp.getLeftChild();
            } else if (temp.getData().compareTo(data) < 0){
                temp = temp.getRightChild();
            }
        }
        return temp;
    }
    

    public void delete(E data){
    	
    Node node = search(data);	
    if (node == null){
    	return;
    }
    else if (node.getLeftChild() != null && node.getRightChild() != null){
    	Node pre = predecessor(node);
    	node.setData(pre.getData());
    	node = pre;
    }
    Node up = leftOf(node) == null ? rightOf(node) : leftOf(node);
    if (up != null){
    	if (node == root){
    		root = node;
    	}
    	else if (node.getParent().getLeftChild() == node){
    		node.getParent().setLeftChild(up);
    	}
    	else {
    		node.getParent().setRightChild(up);
    	}
    	if (isBlack(node)){
    		fixDelete(up);
    	}
    }
    else if (node == root) {
    	root = null;
    }
    else{
    	if(isBlack(node)){
    		fixDelete(node);
    	}
    	removeFromParent(node);
    	
    }
    
    }
    public void traverse(String order, Node<E> top) {
        // Preform a preorder traversal of the tree
    	if (top != null){
                    if (top.getData() != null) {
                        System.out.print(top.getData().toString() + " ");
                        traverse("preorder", top.getLeftChild());
                        traverse("preorder", top.getRightChild());
                    }
                    return;
    	}
    }


    public void rightRotate(Node<E> x){
    	if (x == null){
    		return;
    	}
    		Node y = leftOf(x);
        	x.setLeftChild(rightOf(y));
        	if (rightOf(y) != null){
        		rightOf(y).setParent(x);
        	}
        	y.setParent(parentOf(x));
        	if (parentOf(x) == null){
        		root = y;
        	}
        	else if (x.equals(leftOf(parentOf(x)))){
        		parentOf(x).setLeftChild(y);
        	}
        	else {
        		parentOf(x).setRightChild(y);
        	}
        	y.setRightChild(x);
        	x.setParent(y);
    	}
    
    	

    public void leftRotate(Node<E> x){
    	if (x == null){
    		return;
    	}
    	Node y = rightOf(x);
		x.setRightChild(leftOf(y));
		if (leftOf(y) != null){
			leftOf(y).setParent(x);
		}
		y.setParent(parentOf(x));
		if (parentOf(x) == null){
			root = y;
		}
		else if ( x.equals(x.getParent().getLeftChild())){
			x.getParent().setLeftChild(y);
		}
		else {
			x.getParent().setRightChild(y);
		}
		y.setLeftChild(x);
		x.setParent(y);
     }
    
    
    // HINT: You may want to create extra methods such as fixDelete or fixInsert
    private void fixInsert(Node n){
    	
    	setColor(n, 'R');
    	
    	if (n != null && n != root && isRed(parentOf(n))){
    		
    		if (isRed(siblingOf(parentOf(n)))){
    			setColor(parentOf(n), 'B');
                setColor(siblingOf(parentOf(n)), 'B');
                setColor(grandparentOf(n), 'R');
                fixInsert(grandparentOf(n));
            }
    	}
    	else if (parentOf(n) == leftOf(grandparentOf(n))) {
            if (n == rightOf(parentOf(n))) {
                leftRotate(n = parentOf(n));
            }
            setColor(parentOf(n), 'B');
            setColor(grandparentOf(n), 'B');
            rightRotate(grandparentOf(n));
        }
    	else if (parentOf(n) == rightOf(grandparentOf(n))) {
            if (n == leftOf(parentOf(n))) {
                rightRotate(n = parentOf(n));
            }
            setColor(parentOf(n), 'B');
            setColor(grandparentOf(n), 'R');
            leftRotate(grandparentOf(n));
        }
    	setColor(root, 'B');
    }
        
    private void fixDelete(Node n){
    	
    	while (n != root && isBlack(n)) {
            if (n == leftOf(parentOf(n))) {


                Node sibling = rightOf(parentOf(n));
                if (isRed(sibling)) {
                    setColor(sibling, 'B');
                    setColor(parentOf(n), 'R');
                    leftRotate(parentOf(n));
                    sibling = rightOf(parentOf(n));
                }
                if (isBlack(leftOf(sibling)) && isBlack(rightOf(sibling))) {
                    setColor(sibling, 'R');
                    n = parentOf(n);
                } else {
                    if (isBlack(rightOf(sibling))) {
                        setColor(leftOf(sibling), 'B');
                        setColor(sibling, 'R');
                        rightRotate(sibling);
                        sibling = rightOf(parentOf(n));
                    }
                    setColor(sibling, colorOf(parentOf(n)));
                    setColor(parentOf(n), 'B');
                    setColor(rightOf(sibling), 'B');
                    leftRotate(parentOf(n));
                    n = (Node) root;
                }
            } else {
                
                Node sibling = leftOf(parentOf(n));
                if (isRed(sibling)) {
                    setColor(sibling, 'B');
                    setColor(parentOf(n), 'R');
                    rightRotate(parentOf(n));
                    sibling = leftOf(parentOf(n));
                }
                if (isBlack(leftOf(sibling)) && isBlack(rightOf(sibling))) {
                    setColor(sibling, 'R');
                    n = parentOf(n);
                } else {
                    if (isBlack(leftOf(sibling))) {
                        setColor(rightOf(sibling), 'B');
                        setColor(sibling, 'R');
                        leftRotate(sibling);
                        sibling = leftOf(parentOf(n));
                    }
                    setColor(sibling, colorOf(parentOf(n)));
                    setColor(parentOf(n), 'B');
                    setColor(leftOf(sibling), 'B');
                    rightRotate(parentOf(n));
                    n = (Node) root;
                }
            }
        }
        setColor(n, 'B');
    	
    }
    private void transplant(Node u, Node v){
    	if (u.getParent() == null){
    		root = v;
    	}
    	else if (u == u.getParent().getLeftChild()){
    		u.getParent().setLeftChild(v);
    	}
    	else {
    		u.getParent().setRightChild(v);
    	}
    	v.setParent(u.getParent());
    	
    }
    private Node treeMin(Node x){
    	while (x.getLeftChild() != null){
    		x = x.getLeftChild();
    	}
    	return x;
    }
    //helper functions
    
    private void removeFromParent(Node n){
    	if(n.getParent() != null){
    		if(n.getParent().getLeftChild() == n){
    			n.getParent().setLeftChild(null);
    		}
    		else if (n.getParent().getRightChild() == n){
    			n.getParent().setRightChild(null);
    		}
    		n.setParent(null);
    	}
    }
    
    //Returns the rightmost node in the left subtree.
    private Node predecessor(Node n){
    	Node l = n.getLeftChild();
    	if ( l!= null) {
    		while(l.getRightChild() != null){
    			l = l.getRightChild();
    		}
    	}
    	return l;
    }
    
    private char colorOf(Node n) {
        return n == null ? 'B' : n.getColor();
    }

    private boolean isRed(Node n) {
        return n != null && colorOf(n) == 'R';
    }

    private boolean isBlack(Node n) {
        return n == null || colorOf(n) == 'B';
    }

    private void setColor(Node n, char c) {
        if (n != null)
            n.setColor(c);
    }

    private Node parentOf(Node n) {
        return n == null ? null : (Node) n.getParent();
    }

    private Node grandparentOf(Node n) {
        return (n == null || n.getParent() == null) ? null : (Node) n
                .getParent().getParent();
    }

    private Node siblingOf(Node n) {
        return (n == null || n.getParent() == null) ? null : (n == n
                .getParent().getLeftChild()) ? (Node) n.getParent().getRightChild()
                : (Node) n.getParent().getLeftChild();
    }

    private Node leftOf(Node n) {
        return n == null ? null : (Node) n.getLeftChild();
    }

    private Node rightOf(Node n) {
        return n == null ? null : (Node) n.getRightChild();
    }
}
