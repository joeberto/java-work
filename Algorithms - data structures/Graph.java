
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

 

public class Graph 
{
	
	public int number;
    public int vertices;
    public int[] vlist;
    public int[][] adjacency_matrix;
    
    int numberofpaths;
    int longestpath;
    int shortestpath;
    int source;
	int numnodes;
	int[] distances;
    
	Set<Integer> unsettled = new HashSet<Integer>();
	Set<Integer> settled = new HashSet<Integer>();

    public Graph(int i){
    	number = i;
    }
    
    public void setGraph(int v) 
    {
        vertices = v;
        vlist = new int[vertices];
        adjacency_matrix = new int[vertices + 1][vertices + 1];
        //populate vlist with vertices 1-N
        for (int i=0; i< vertices; i++){
        	vlist[i] = i+1;
        }
        //source is always first node
        source = vlist[0];
        numnodes = vertices;
        distances = new int[numnodes+1];
    }
    
    public void addEdge(int to, int from, int edge) 
    {
        try 
        {
            adjacency_matrix[from][to] = edge;
        }
        catch (ArrayIndexOutOfBoundsException index) 
        {
            System.out.println("Vertices DNE");
        }
    }
 
    public int getEdge(int to, int from) 
    {
        try 
        {
            return adjacency_matrix[from][to];
        }
        catch (ArrayIndexOutOfBoundsException index) 
        {
            System.out.println("Vertices DNE");
        }
        return -1;
    }
    
    public void printGraph(){
    	System.out.println("The adjacency matrix for graph " + number + " is: ");
        System.out.print("  ");
        for (int i = 1; i <= vertices; i++)
            System.out.print(i + " ");
        System.out.println();

        for (int i = 1; i <= vertices; i++) 
        {
            System.out.print(i + " ");
            for (int j = 1; j <= vertices; j++) 
                System.out.print(this.getEdge(i, j) + " ");
            System.out.println();
        }
    }
   
    public void numberOfPaths(){
    	
    	//length is vertices + 1 to avoid zero index
    	int N = vertices+1;
    	int[] numPath = new int[N];
    	int[] mindistances = new int[N];
    	int[] maxdistances = new int[N];
  
    	//initialize min/max distances to INF, except source
    	for(int i=1; i< N; i++){
    		mindistances[i] = 999999;
    		maxdistances[i] = -99999;
    	}
    	mindistances[1] = 0;
    	maxdistances[1] = 0;
    	
    	numPath[1] = 1;
    	for (int i=2; i < N; i++){
    		numPath[i] = 0;
    	}
    	
    	for(int i=1; i < N-1; i++){
    		for(int j=i+1; j < N; j++){
    			//if (i,j) is an edge (ie = 1)
    			if (adjacency_matrix[j][i] == 1){
    				numPath[j] = numPath[i] + numPath[j];
    				//update longest path and shortest path values herce
    				if(mindistances[j] > mindistances[i] +1 ){
    					mindistances[j] = mindistances[i] +1;
    				}
    				if (maxdistances[j] < maxdistances[i] +1){
    					maxdistances[j] = maxdistances[i] +1;
    				}
    				
    			}
    		}
    	}
    	//print paths
    	shortestpath = mindistances[N-1];
    	longestpath = maxdistances[N-1];
    	numberofpaths = numPath[N-1]; 
    	//System.out.println("Number of paths from 1 to 5: " + numPath[N-1]);
    	//System.out.println("Shortest path from 1 to 5: " + mindistances[N-1]);
    }
    
    public void printstuff(){
    	
    	System.out.println("graph number: " + number);
    	System.out.println("Shortest path: " + shortestpath);
    	System.out.println("Longest path: " + longestpath);
    	System.out.println("Number of paths: " + numberofpaths);
    	System.out.println("");
    }
    
    public static void main(String args[]) 
    {
        int to;
        int from;
        int i, j;
        int numgraphs;
        int vertices, edges;
        ArrayList<Graph> graphlist;
       
        //scanner to take stdio
		Scanner scan = new Scanner(System.in);
		
		//read first line for number of graphs
		numgraphs = scan.nextInt();
		
		//array list to store graphs
		graphlist = new ArrayList<Graph>(numgraphs);
		
		for(i = 1; i < numgraphs+1; i++){
			//create number of graphs
			graphlist.add(new Graph(i));
		}
		//next line to skip over missed newline char
		scan.nextLine();
		//for number of graphs, do:
		for (i =0; i < numgraphs; i++){
			//read second line for node count
			vertices = scan.nextInt();
			graphlist.get(i).setGraph(vertices);
			scan.nextLine();
			edges = scan.nextInt();
			scan.nextLine();
			//for all the adjs
			for (j=0; j < edges; j++){
				//add edges
				String input = scan.nextLine();
				to = Integer.parseInt(input.split(" ")[0]);
				from = Integer.parseInt(input.split(" ")[1]);
				graphlist.get(i).addEdge(to, from, 1);
							
			}
		}
		scan.close();
		//graph 1 stored at graphlist[0].
		for(int k=0; k< graphlist.size(); k++){
			graphlist.get(k).numberOfPaths();
			graphlist.get(k).printstuff();
		}
		
        
}
}

 
    	
    
 
    
		

