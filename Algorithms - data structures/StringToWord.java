import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class StringToWord {
	
	public static Set<String> dictionary = new HashSet<String>();
	
	
	public static void main(String[] args){
		
	loadDictionary("diction10k.txt");	
	//file for testing
    //File file = new File("inSampleSmall.txt");
    //scanner to take stdio
	Scanner scanner = null;
	
	
	scanner = new Scanner(System.in);

	//read first line for number of strings
	int numstrings = scanner.nextInt();
	
	//line to skip over missed newline char
	scanner.nextLine();
	//for number of strings, do:
	for (int j =0; j < numstrings; j++){

		int phase = j+1;
		String string = scanner.nextLine();	
		System.out.println("phase number: " + phase);
		System.out.println(string);
		System.out.println();
		System.out.println("iterative attempt:");
		SplitIter(string);
		System.out.println();
		System.out.println("memoized attempt:");
		SplitRecur(string);
		System.out.println();
		}
	
	scanner.close();
}
	static void SplitRecur(String input){
		//array of dirrent sentences
		ArrayList<String> sentences = new ArrayList<String>();
		//array for our memoization
		ArrayList<String> memo = new ArrayList<String>(); 
		SplitRecurFunc(input, input.length(), "", memo, sentences);
		
		//check if any sentences were found
		if(sentences.size() > 0){
			System.out.println("YES, can be split");
			System.out.println(sentences.get(0));
			return;
		}
		System.out.println("NO, cannot be split");
	}
	static void SplitRecurFunc(String input, int length, String suffix, ArrayList<String> memo, ArrayList<String> sentences){
				//check if input has been computed before
				if(memo.contains(input)){
					//it has, add input to suffix and add to sentences
					suffix += input;
					sentences.add(suffix);
					return;
				}
				//input is not a word that has been computed before
				for (int i = 1; i <= length; i++){
					//prefix is from start of input to i
					String prefix = input.substring(0,i);
					//if the prefix is a word
					if( dictionary.contains(prefix)){
						//add it to our memo 
						memo.add(prefix);
						//if were at the end of the input
						if(i == length){
							//add to list of words
							suffix += prefix;
							sentences.add(suffix);
							return;
						}
						// if were not at end of input, keep checking
						SplitRecurFunc(input.substring(i,length), length-i, suffix+prefix+" ", memo, sentences);	
							
						}
					
					}
			
				}
	
	
	static void SplitIter(String input){
		//get length of input 
		int inlength = input.length();
		//words keeps track of all the possible words that start at a certain index
		ArrayList<String> [] words = new ArrayList[inlength+1];
		//initialize 0th spot
		words[0] = new ArrayList<String>();
		
	   //for length of input
	   // i is where a word may start
	   for(int i=0; i<inlength; i++){
		    //make sure spot were working on isn't null
	        if(words[i]!=null){
	        	//j is where a word may end- start it at i+1
	            for(int j=i+1; j<=inlength; j++){
	            	//iterating through string to get current prefix
					String prefix = input.substring(i,j);
	                //if prefix is a word
	                if(dictionary.contains(prefix)){
	                	//if the prefix is a new word that exists at the same index j as another word
	                	if(words[j]!=null){
	                		//add the other word at index j
	                        words[j].add(prefix);	                        	
	                     	}
	                	//else if there is no other words at index j in our words list, add it
	                    if(words[j]==null){
	                    	   //make an Arraylist to store all words at index j
		                       ArrayList<String> newWords = new ArrayList<String>();
		                       //add the new word to our newWords list
		                       newWords.add(prefix);
		                       //add the list of new words to our words list
		                       words[j]=newWords;
	                    }
	                }
	            }
	        }
}
	    //checks if there are any words at end of input string
	    if(words[inlength]==null){
	    	System.out.println("NO, cannot be split");
	    	return;
	    }
	    //else there are valid words that are length of input
    	//arraylist to hold different sentences - only need one
        ArrayList<String> possbleSentences = new ArrayList<String>();
        //depth first search builds our different sentences using list of words
        SentenceSearch(possbleSentences, words, "", inlength);
        System.out.println("YES, can be split");
        System.out.println(possbleSentences.get(0));
        return;
	        
	    
	}

	//depth first search function to build sentences
	static public void SentenceSearch (ArrayList<String> suffix, ArrayList<String> [] words, String current, int n){
		//if n is not 0, still more words to search
		if(n!=0){
			//for each string in our array of words
			for(String str: words[n]){
				//add current word to our sentence sum
				String sum = str + " " + current;
				//continue building the sentence
				SentenceSearch(suffix, words, sum, n-str.length());
				return;
			}
		}
		//else were at the end, at the current string to our total suffix
		else{
			suffix.add(current);
			return;
		}
	}
	
	public static void loadDictionary(String dictionaryFileName)
	{
	File inFile = new File(dictionaryFileName);
	try {
	Scanner scan = new Scanner(inFile);
	String line;
	 while (scan.hasNext()) {
	 line = scan.next();
	 dictionary.add(line.trim());
	 }//while
	 scan.close();
	} //try block
	catch (FileNotFoundException e) {
	 e.printStackTrace();
	 }
	}//load dictionary

}

