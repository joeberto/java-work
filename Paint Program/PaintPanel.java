import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

public class PaintPanel extends JPanel {
	private final ArrayList<PaintPoint> _points;
	
	private Color _currentColor;
	private int _currentSize = 7;
	
	
	public PaintPanel() {
		_points = new ArrayList<>();
		
		
		MouseAdapter adapter = new MouseAdapter() {
			@Override
		    public void mouseDragged(MouseEvent e) {
				PaintPoint point = new PaintPoint(e.getX(), e.getY(),
						_currentColor, _currentSize);
				_points.add(point);
				repaint();
			}
		};
		
		addMouseListener(adapter);
		addMouseMotionListener(adapter);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		for (PaintPoint point : _points) {
			point.draw(g);
		}
	}
	

	
	public void setColor(Color color) {
		_currentColor = color;
	}

	public void setSize(int dotSize) {
		_currentSize = dotSize;
	}
	
	public void clearCanvas() {
		_points.clear();
		repaint();
	}
	
}
