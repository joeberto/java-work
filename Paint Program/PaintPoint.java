import java.awt.Color;
import java.awt.Graphics;

public class PaintPoint {
	
	
	private final int _size; 
	private final int _x;
	private final int _y;
	private final Color _color;
	
	public PaintPoint(int x, int y, Color color, int size) {
		_x = x;
		_y = y;
		_color = color;
		_size = size;
	}
	
	public int getX() {
		return _x;
	}

	public int getY() {
		return _y;
	}

	public Color getColor() {
		return _color;
	}
	
	
	public void draw(Graphics g) {
		g.setColor(_color);
		g.fillOval(_x, _y, _size, _size);
		
	
		}
	}

