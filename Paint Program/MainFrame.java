import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame extends JFrame {
	private final PaintPanel _paintPanel;
	
	public MainFrame() {
		super("Express Yo Self");
		setLayout(new BorderLayout());
		
		_paintPanel = new PaintPanel();
		_paintPanel.setPreferredSize(new Dimension(600, 400));
		add(_paintPanel, BorderLayout.CENTER);
		
		JButton blackButton = new JButton("Black");
		blackButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.setColor(Color.black);			
			}			
		});
		
		JButton greenButton = new JButton("Green");
		greenButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.setColor(Color.green);			
			}			
		});
		
		JButton yellowButton = new JButton("Yellow");
		yellowButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.setColor(Color.yellow);			
			}			
		});
		
		JButton grayButton = new JButton("Gray");
		grayButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.setColor(Color.gray);			
			}			
		});
		
		//Buttons on right side
		JButton smallButton = new JButton("Small");
		smallButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.setSize(7);			
			}			
		});
		
		JButton mediumButton = new JButton("Medium");
		mediumButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.setSize(14);				
			}			
		});
		
		JButton largeButton = new JButton("Large");
		largeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.setSize(20);				
			}			
		});
		
		JButton clearButton = new JButton("Clear");
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_paintPanel.clearCanvas();			
			}			
		});
		
		
		JPanel colorButtonPanel = new JPanel(new GridLayout(0,1));
		colorButtonPanel.add(blackButton);
		colorButtonPanel.add(greenButton);
		colorButtonPanel.add(yellowButton);
		colorButtonPanel.add(grayButton);
		
		JPanel sizeButtonPanel = new JPanel(new GridLayout(0,1));
		sizeButtonPanel.add(smallButton);
		sizeButtonPanel.add(mediumButton);
		sizeButtonPanel.add(largeButton);
		sizeButtonPanel.add(clearButton);
		
		

		JPanel leftButtonPanel = new JPanel(new GridLayout(0,1));
		leftButtonPanel.add(colorButtonPanel);
		add(leftButtonPanel, BorderLayout.WEST);
		
		JPanel rightButtonPanel = new JPanel(new GridLayout(0,1));
		rightButtonPanel.add(sizeButtonPanel);
		add(rightButtonPanel, BorderLayout.EAST);
	}
}
