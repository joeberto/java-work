
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

//a TimeChunk is created every time is press "start" on application"
public class TimeChunk {

	float hours,minutes,seconds;
	LocalTime startTime;
	LocalTime endTime;
	//full date-time of this chunk - should get called on creation of timechunk
	LocalDateTime startDateTime = LocalDateTime.now();
	//format
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	String date = ((startDateTime.format(formatter)).split(" "))[0];
	
	//When we start timer, must pass in current time
	public void startTimer() {
		startTime = LocalTime.now();
	}
	//When we end timer, get current time and find diff between start
	public void endTimer() {
		endTime = LocalTime.now();
		seconds = Duration.between(startTime, endTime).getSeconds();
		minutes = seconds/60;
		hours = minutes/60;
	}
	
}

