import java.util.ArrayList;

public class Project {

		//Name of this project
		private String name;
		//Points to associated client for this project.
		private Client client;
		//List of timechunks
		private ArrayList<TimeChunk> timeChunkList = new ArrayList<TimeChunk>();
		//total minutes spent on this project
		private float totalMinutes;
		
		//constructor for loading data
		Project(String projName, Client prevClient, ArrayList<TimeChunk> prevTimeChunkList){
			name = projName;
			client = prevClient;
			timeChunkList = prevTimeChunkList;
			calcTotalMin();
		}
		
		Project(String projName, Client c){
			name = projName;
			client = c;
		}
		
		Project(String n){
			name = n;
		}
		
		private void calcTotalMin() {
			for (TimeChunk chunk : timeChunkList) {
				totalMinutes += chunk.minutes;
			}
		}
		
		public TimeChunk startSession() {
			
			TimeChunk session = new TimeChunk();
			timeChunkList.add(session);
			session.startTimer();
			return session;
		}
		
		public void endSession(TimeChunk session) {
			
			session.endTimer();
		}
		
		public String getName() {
			return name;
		}
		
		public float getMinutes() {
			calcTotalMin();
			return totalMinutes;
		}
		
		@Override
		public String toString() {
			return name;
		}
}	
