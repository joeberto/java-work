import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

public class Client {

		//name of client
		String name;
		//list of projects associated with client
		ArrayList<Project> projectList = new ArrayList<Project>();
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
		
		Client(String n){
			name = n;
		}
		
		public DefaultComboBoxModel<String> getModel() {
			return model;
		}
		
		public void addProject(Project newProj) {

			projectList.add(newProj);
			model.addElement(newProj.getName());
		}
		
		public Project getProject(String projName) {
			
			for (Project proj : projectList) {
				if (proj.getName() == projName) {
					return proj;
				}
			}
			Project dummy = new Project("dummy");	
			System.out.println("Returned null project");
			return dummy;
		}
		
		@Override
		public String toString() {
			return name;
		}
}

