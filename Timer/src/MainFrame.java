import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.awt.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*; 

public class MainFrame extends JFrame {
	
	ArrayList<Client> clientList = new ArrayList<Client>();
    Client currentClient;
	Project currentProject;
	String clientStringList[] = {};
    String projectStringList[] = {};
    Point currentFrameLoc = this.getLocation();
    boolean timing = false;
    boolean isProjectSelected = false;
    TimeChunk currChunk;
    
	//new client button
	public MainFrame() {
		
		super("Timer");
		this.setPreferredSize(new Dimension(500,400));
		//this.setResizable(false);
		this.setLayout(new GridLayout());
		
		//Models and combo boxes
		DefaultComboBoxModel<String> clientModel = new DefaultComboBoxModel<String>();
		DefaultComboBoxModel<String> projectModel = new DefaultComboBoxModel<String>();
		JComboBox<String> clientBox = new JComboBox<>(clientModel);
		JComboBox<String> clientBox2 = new JComboBox<>(clientModel);
		JComboBox<String> projectBox = new JComboBox<>(projectModel);
		clientBox.setPreferredSize(new Dimension(350,10));
		//clientBox.setPrototypeDisplayValue("                                   ");

		
		//Popup windows
		JDialog newClientFrame = new JDialog(this);
		JDialog newProjectFrame = new JDialog(this);	
		newClientFrame.setPreferredSize(new Dimension (400,150));
		newProjectFrame.setPreferredSize(new Dimension (400, 150));
		newClientFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		newProjectFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		newClientFrame.setTitle("New Client");
		newProjectFrame.setTitle("New Project");
		JTextField ncText = new JTextField(20);
		JTextField npText = new JTextField(20);
		npText.setText("Project name");
		ncText.setText("Enter Name");
		JButton newClientButton = new JButton("Add");
		JButton newProjectButton = new JButton("Create");
		JPanel ncwPanel = new JPanel(new GridLayout(0,1));
		JPanel npwPanel = new JPanel(new GridLayout(0,1));
		JPanel npwMainPanel = new JPanel();
		
		ncwPanel.add(ncText);
		npwPanel.add(npText);
		ncwPanel.add(newClientButton);
		npwPanel.add(clientBox2);
		npwPanel.add(npText);
		npwPanel.add(newProjectButton);
		npwMainPanel.add(npwPanel);
		newClientFrame.add(ncwPanel);
		newProjectFrame.add(npwMainPanel);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu newMenu = new JMenu("New..");
		menuBar.add(newMenu);
		JMenuItem newClientMenu = new JMenuItem("Client", KeyEvent.VK_T);
		JMenuItem newProjectMenu = new JMenuItem("Project", KeyEvent.VK_T);
		newMenu.add(newClientMenu);
		newMenu.add(newProjectMenu);
		
		//end popup windows
		
		//JToggleButton button = new JToggleButton(UIManager.getIcon("OptionPane.informationIcon"));
        //button.setSelectedIcon(UIManager.getIcon("OptionPane.errorIcon"));

		//stuff for custom buttons
		BufferedImage theButtonImage1 = null;
		BufferedImage theButtonImage2 = null;
		BufferedImage stopButtonImage1 = null;
		BufferedImage stopButtonImage2 = null;

		
		try {
			theButtonImage1 = ImageIO.read(new File("src\\images\\start-btn.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			theButtonImage2 = ImageIO.read(new File("src\\images\\start-btn-hover.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			stopButtonImage1 = ImageIO.read(new File("src\\images\\stop-btn.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			stopButtonImage2 = ImageIO.read(new File("src\\images\\stop-btn-hover.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		final ImageIcon theButtonIcon = new ImageIcon(theButtonImage1);
		final ImageIcon theButtonIconHover = new ImageIcon(theButtonImage2);
		final ImageIcon stopButtonIcon = new ImageIcon(stopButtonImage1);
		final ImageIcon stopButtonIconHover = new ImageIcon(stopButtonImage2);

		JToggleButton theButton = new JToggleButton(theButtonIcon);
		theButton.setBorder(BorderFactory.createEmptyBorder());
		theButton.setContentAreaFilled(false);
		theButton.setSelectedIcon(stopButtonIcon);
		theButton.setRolloverEnabled(true);
		theButton.setRolloverIcon(theButtonIconHover);
		theButton.setRolloverSelectedIcon(stopButtonIconHover);
		theButton.setCursor(new Cursor(Cursor.HAND_CURSOR));

		JLabel elapsedTime = new JLabel("0:00");
		JLabel clientLabel = new JLabel("Client:");
		JLabel projectLabel = new JLabel("Project:");
		JPanel leftPanel = new JPanel(new GridLayout(0,1));
		JPanel rightPanel = new JPanel(new GridLayout(0,1));
		JPanel mainPanel = new JPanel();

		//add all our components up
		leftPanel.add(clientLabel);
		leftPanel.add(clientBox);
		leftPanel.add(projectLabel);
		leftPanel.add(projectBox);
		rightPanel.add(theButton);
		rightPanel.add(elapsedTime);
		mainPanel.add(leftPanel);
		mainPanel.add(rightPanel);
		this.add(mainPanel);
		this.setJMenuBar(menuBar);

		
		
		
		
		//when selecting a client..
		clientBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	//load projectBox from selected client 
            	projectBox.setModel(getClient(clientBox.getSelectedItem().toString()).getModel());
            }
          });
		
		//when selecting a project..
		projectBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	currentProject = currentClient.getProject(projectBox.getSelectedItem().toString());
            }
          });
		
		//when clicking on New>Client...
        newClientMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	
            	//make the window visible
                newClientFrame.pack();
                newClientFrame.setVisible(true);}
          });
		//when clicking on New>Project...
        newProjectMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	//make the window visible
                newProjectFrame.pack();
                newProjectFrame.setVisible(true);}
          });
		
		//new client text box
		ncText.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent e){
                ncText.setText("");
            }
            @Override
            public void focusLost(FocusEvent arg0) {
            	 //ncText.setText("Client Name");
            }
        });
		
		//new project text box
		npText.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent e){
                npText.setText("");
            }
            @Override
            public void focusLost(FocusEvent arg0) {
            	//npText.setText("Project name");
            }
        });
		
		//Clicking on add new client button
		newClientButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(ncText.getText().isEmpty()) {
					ncText.setText("Enter name");
					return;
				}
				
				if (!clientList.contains(getClient(ncText.getText()))) {
					Client newClient = new Client(ncText.getText());
					clientList.add(newClient);
					currentClient = newClient;
					clientModel.addElement(newClient.name);
					newClientFrame.setVisible(false);
				}
				else {
					ncText.setText("Client already exists");
				}
				
							
			}			
		});
		
		newProjectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//check if any clients exist
				if (clientList.isEmpty()) {
					npText.setText("Client needed");
				}
				//we good, get the selected client
				else {
					currentClient = getClient(clientBox2.getSelectedItem().toString());
					//now create and attach new project to this client
					Project newProject = new Project(npText.getText(), currentClient);
					currentClient.addProject(newProject);
					projectModel.addElement(newProject.getName());
					newProjectFrame.setVisible(false);
				}	
			}			
		});
		

		theButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!timing) {
				currChunk = currentProject.startSession();
				timing = true;
				}
				else if (timing) {
					currentProject.endSession(currChunk);
					timing = false;
					System.out.println("Total time: ");
					System.out.println(currentProject.getMinutes());
				}
			}			
		});
	}
	
	public boolean findClient(String name) {
		for (Client c : clientList) {
			if (name == c.name) {
				return true;
			}
		}
		return false;
	}
	
	public Client getClient(String name) {
		Client dummy = new Client("dummy");
		for (Client c : clientList) {
			if (name == c.name) {
				return c;
			}
		}
		return dummy;
	}
	
	public static String[] add(String[] originalArray, String newItem)
	{
	    int currentSize = originalArray.length;
	    int newSize = currentSize + 1;
	    String[] tempArray = new String[ newSize ];
	    for (int i=0; i < currentSize; i++)
	    {
	        tempArray[i] = originalArray [i];
	    }
	    tempArray[newSize- 1] = newItem;
	    return tempArray;   
	}
	
}



	

		
		

