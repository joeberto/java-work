import java.util.Comparator;
import java.util.ArrayList;
public class Data {
  private String firstName;
  private String lastName;
  private String address;
  private String addressTwo;
  private String city;
  private String state;
  private String zipCode;
  private String phoneNumber;
  private String email;
  public Data(String f, String l, String a, String a2, String ci, String s, String z, String p, String e) {
    firstName = f;
    lastName = l;
    address = a;
    addressTwo = a2;
    city = ci;
    state = s;
    zipCode = z;
    phoneNumber = p;
    email = e;
  }
  public String getSearchString() {
    String rs = (firstName.length() != 0) ? firstName + " " : "";
    rs += (lastName.length() != 0) ? lastName + " " : "";
    rs += (address.length() != 0) ? address + " " : "";
    rs += (addressTwo.length() != 0) ? addressTwo + " " : "";
    rs += (city.length() != 0) ? city + " " : "";
    rs += (state.length() != 0) ? state + " " : "";
    rs += (zipCode.length() != 0) ? zipCode + " " : "";
    rs += (phoneNumber.length() != 0) ? phoneNumber + " " : "";
    rs += (email.length() != 0) ? email + " " : "";
    rs = rs.toLowerCase();
    return rs;
  }
  public String formatData() {
    String s = (lastName.equals("")) ? firstName : lastName + ", " + firstName;
    s += (zipCode.equals("")) ? "" : " " + zipCode;
    return s;
  }
  public String formatDataForFile() {
    String s = lastName + ":" + firstName + ":" + address + ":" + addressTwo + ":" + city + ":" + state + ":" + zipCode + ":" + phoneNumber + ":" + email + ":end\r\n";
    return s;
  }
  // Public get functions
  public String getFirstName() {
    return firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public String getAddress() {
    return address;
  }
  public String getAddressTwo() {
    return addressTwo;
  }
  public String getCity() {
    return city;
  }
  public String getState() {
    return state;
  }
  public String getZipCode() {
    return zipCode;
  }
  public String getPhoneNumber(){
      return phoneNumber;
  }
  public String getEmail(){
      return email;
  }
  //Public set functions
  public void setFirstName(String f) {
    firstName = f;
}
  public void setLastName(String l) {
    lastName = l;
  }
  public void setAddress(String a) {
    address = a;
  }
  public void setAddressTwo(String a) {
    addressTwo = a;
  }
  public void setCity(String c) {
    city = c;
  }
  public void setState(String s) {
    state = s;
  }
  public void setZipCode(String z) {
    zipCode = z;
  }
  public void setPhoneNumber(String p){
    phoneNumber = p;
  }
  public void setEmail(String e){
    email = e;
  }
}