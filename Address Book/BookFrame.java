import java.io.File;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
public class BookFrame extends JFrame {
  private JLabel errorLabel;
  private AddressHandler addresses;
  private DefaultListModel<String> model;
  private int index;
  //search function for JList
  public void filterModel(DefaultListModel<String> model, String filter, AddressHandler addresses) {
      //this is what the model has
      String ms;
      //this is the full string of data we want to search
      String fs;
      //case insensitive
      filter = filter.toLowerCase();
      ArrayList<Data> addressList = addresses.getArrayList();
      for (Data d : addressList) {
          ms = d.formatData();
          fs = d.getSearchString();
          if (!fs.contains(filter) ) {
              if (model.contains(ms)) {
                  model.removeElement(ms);
                  
              }
          } else {
              if (!model.contains(ms)) {
                  model.addElement(ms);
              }
          }
      }
  }
  public BookFrame(String path) {
        //Create and set up the window.
        super("Address Book");
        this.setPreferredSize(new Dimension(700, 400));
        this.setResizable(false);
        this.setLayout(new BorderLayout());
        addresses = new AddressHandler();
        //Menu Bar
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
        JMenuItem menuNew = new JMenuItem("New", KeyEvent.VK_T);
        JMenuItem menuOpen = new JMenuItem("Open", KeyEvent.VK_T);
        JMenuItem menuClose = new JMenuItem("Close", KeyEvent.VK_T);
        JMenuItem menuSave = new JMenuItem("Save", KeyEvent.VK_T);
        JMenuItem menuSaveAs = new JMenuItem("Save As...", KeyEvent.VK_T);
        JMenuItem menuQuit = new JMenuItem("Quit all", KeyEvent.VK_T);
        fileMenu.add(menuNew);
        fileMenu.add(menuOpen);
        fileMenu.add(menuClose);
        fileMenu.addSeparator();
        fileMenu.add(menuSave);
        fileMenu.add(menuSaveAs);
        fileMenu.addSeparator();
        fileMenu.add(menuQuit);
        //Labels
        //Spaces are a super hacky way to get label to align correctly w/ field box
        JLabel lastLabel = new JLabel("Last Name");
        JLabel firstLabel = new JLabel("First Name                  ");
        JLabel address1Label = new JLabel("Address Line 1");
        JLabel address2Label = new JLabel("Address Line 2");
        JLabel cityLabel = new JLabel("City                              ");
        JLabel stateLabel = new JLabel("State");
        JLabel zipLabel = new JLabel("Zip Code");
        JLabel phoneLabel = new JLabel("Phone Number          ");
        JLabel emailLabel = new JLabel("Email");
        errorLabel = new JLabel("");
        //Text entry boxes
        JTextField lastField = new JTextField(10);
        JTextField firstField = new JTextField(10);
        JTextField address1Field = new JTextField(15);
        JTextField address2Field = new JTextField(15);
        JTextField cityField = new JTextField(10);
        JTextField stateField = new JTextField(2);
        JTextField zipField = new JTextField(5);
        JTextField phoneField = new JTextField(10);
        JTextField emailField = new JTextField(12);
        //Buttons
        JButton newButton = new JButton("New");
        JButton editButton = new JButton("Edit");
        JButton deleteButton = new JButton("Delete");
        JButton zipSort = new JButton("Sort by ZIP");
        JButton nameSort = new JButton("Sort by Name");
        //Panels to contain Labels and text entry boxes
        JPanel nameLabelPanel = new JPanel(new FlowLayout(5, 20,1));
        JPanel nameFieldPanel = new JPanel(new FlowLayout(5, 20,1));
        JPanel addressLabelPanel1 = new JPanel(new FlowLayout(5, 20,1));
        JPanel addressFieldPanel1 = new JPanel(new FlowLayout(5, 20,1));
        JPanel addressLabelPanel2 = new JPanel(new FlowLayout(5, 20,1));
        JPanel addressFieldPanel2 = new JPanel(new FlowLayout(5, 20,1));
        JPanel addressLabelPanel3 = new JPanel(new FlowLayout(5, 20,1));
        JPanel addressFieldPanel3 = new JPanel(new FlowLayout(5, 20,1));
        JPanel phoneEmailLabelPanel = new JPanel(new FlowLayout(5, 20,1));
        JPanel phoneEmailFieldPanel = new JPanel(new FlowLayout(5, 20,1));
        JPanel errorPanel = new JPanel(new FlowLayout(5, 20,1));
        //Panel for buttons
        JPanel buttonPanel = new JPanel(new FlowLayout(5, 20,1));
        buttonPanel.add(newButton);
        buttonPanel.add(editButton);
        buttonPanel.add(deleteButton);
        JPanel sortPanel = new JPanel(new GridLayout(1, 1));
        sortPanel.add(zipSort);
        sortPanel.add(nameSort);
        sortPanel.setSize(200, 100);
        //This panel will contain the panels above
        JPanel allPanel = new JPanel();
        allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.Y_AXIS));
        //Jlist for listing entries
        
        model = new DefaultListModel<>();
        JList<String> entryList = new JList<>(model);
        JScrollPane entryPane = new JScrollPane(entryList);
        entryPane.setMinimumSize(new Dimension(300, 400));
        entryPane.setMaximumSize(new Dimension(300, 400));
        //Search bar for Jlist
        JTextField searchField = new JTextField(15);
        searchField.getDocument().addDocumentListener(new DocumentListener(){
            @Override public void insertUpdate(DocumentEvent e) { filter(); }
            @Override public void removeUpdate(DocumentEvent e) { filter(); }
            @Override public void changedUpdate(DocumentEvent e) {}
            private void filter() {
                String filter = searchField.getText();
                filterModel((DefaultListModel<String>)model, filter, addresses);
            }
        });
        searchField.setToolTipText("Search by field...");
        searchField.setText("Search by field...");
        searchField.setSize(new Dimension(300, 10));
        sortPanel.add(searchField);
        //Removes "Search.." when user clicks on search bar
        searchField.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent e){
                searchField.setText("");
            }
            @Override
            public void focusLost(FocusEvent arg0) {
                // TODO Auto-generated method stub
            }
        });
        //add fields and labels to each panel
        //name labels
        nameLabelPanel.add(firstLabel);
        nameLabelPanel.add(lastLabel);
        //name fields
        nameFieldPanel.add(firstField);
        nameFieldPanel.add(lastField);
        //address line 1 label and field
        addressLabelPanel1.add(address1Label);
        addressFieldPanel1.add(address1Field);
        //address line 2 label and field
        addressLabelPanel2.add(address2Label);
        addressFieldPanel2.add(address2Field);
        //address 3 label
        addressLabelPanel3.add(cityLabel);
        addressLabelPanel3.add(stateLabel);
        addressLabelPanel3.add(zipLabel);
        //address 2 field
        addressFieldPanel3.add(cityField);
        addressFieldPanel3.add(stateField);
        addressFieldPanel3.add(zipField);
        //phone+email labels
        phoneEmailLabelPanel.add(phoneLabel);
        phoneEmailLabelPanel.add(emailLabel);
        //phone+email fields
        phoneEmailFieldPanel.add(phoneField);
        phoneEmailFieldPanel.add(emailField);
        //error msg area
        errorPanel.add(errorLabel);
        //add to main panel
        allPanel.add(nameLabelPanel);
        allPanel.add(nameFieldPanel);
        allPanel.add(addressLabelPanel1);
        allPanel.add(addressFieldPanel1);
        allPanel.add(addressLabelPanel2);
        allPanel.add(addressFieldPanel2);
        allPanel.add(addressLabelPanel3);
        allPanel.add(addressFieldPanel3);
        allPanel.add(phoneEmailLabelPanel);
        allPanel.add(phoneEmailFieldPanel);
        allPanel.add(buttonPanel);
        allPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
        allPanel.add(errorPanel);
        //panel for entries list and search bar, and console?
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout());
        leftPanel.add(entryPane,BorderLayout.CENTER);
        leftPanel.add(searchField,BorderLayout.SOUTH);
        leftPanel.add(sortPanel,BorderLayout.NORTH);
        //add panels to main this
        JSplitPane splitPane = new JSplitPane();
        splitPane.setLeftComponent(leftPanel);
        splitPane.setRightComponent(allPanel);
        splitPane.setEnabled(false);
        //Split Pane contains entries on left and fields on right
        this.add(splitPane);
        this.setJMenuBar(menuBar);
        /* Buttons */
        // "New" button action listener
        // Verifies information and adds to address book
        newButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
              String l = lastField.getText();
              String f = firstField.getText();
              String a = address1Field.getText();
              String a2 = address2Field.getText();
              String ci = cityField.getText();
              String st = stateField.getText();
              String z = zipField.getText();
              String p = phoneField.getText();
              String em = emailField.getText();
              if (!isValidEntry(l, f, a, a2, ci, st, z, p, em)) {
                return;
              }
              Data d = new Data(f, l, a, a2, ci, st, z, p, em);
              addresses.addAddress(d);
              //String s = addresses.formatAddresses();
              String s = d.formatData();
              model.addElement(s);
              //System.out.printf("%d\n", entryList.getSize() );
                //empty the entry after finished
                lastField.setText("");
                firstField.setText("");
              address1Field.setText("");
              address2Field.setText("");
              cityField.setText("");
              stateField.setText("");
              zipField.setText("");
              phoneField.setText("");
              emailField.setText("");
          }
        });
            // "Edit" button action listener
        //  Edits the highlighted entry from the address book
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = entryList.getSelectedIndex();
                if(selectedIndex != -1){
                  String l = lastField.getText();
                  String f = firstField.getText();
                  String a = address1Field.getText();
                  String a2 = address2Field.getText();
                  String ci = cityField.getText();
                  String st = stateField.getText();
                  String z = zipField.getText();
                  String p = phoneField.getText();
                  String em = emailField.getText();
                  if (!isValidEntry(l, f, a, a2, ci, st, z, p, em)) {
                    return;
                  }
                  Data d = addresses.getElement(selectedIndex);
                  //Set any fields that have been changed
                  d.setLastName(l);
                  d.setFirstName(f);
                  d.setAddress(a);
                  d.setAddressTwo(a2);
                  d.setCity(ci);
                  d.setState(st);
                  d.setZipCode(z);
                  d.setPhoneNumber(p);
                  d.setEmail(em);
                  d.formatData();
                  //Update entry
                  model.set(selectedIndex, d.formatData());
                }
            }
        });
       // "Delete" button action listener
       //  Deletes a highlighted entry from the address book
       deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = entryList.getSelectedIndex();
                if(selectedIndex != -1){
                    Data d = addresses.getElement(selectedIndex);
                    model.remove(selectedIndex);
                    addresses.removeAddress(d);
                    //empty the entry after finished
                        lastField.setText("");
                        firstField.setText("");
                    address1Field.setText("");
                    address2Field.setText("");
                    cityField.setText("");
                    stateField.setText("");
                    zipField.setText("");
                    phoneField.setText("");
                    emailField.setText("");
                }
            }
          });
       // "Sort by Name" button action listener
       // Sorts the data by name
       nameSort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addresses.sortName();
                refreshList();
            }
          });
       // "Sort by Zip" button action listener
       // Sorts the data by zip
       zipSort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addresses.sortZip();
                refreshList();
            }
          });
       // Entry List mouse Listener
       // Fills in info when clicking an entry
       entryList.addMouseListener(new MouseAdapter(){
           public void mouseClicked(MouseEvent e){
           try {
             if(e.getClickCount() == 1){

                     int selectedIndex = entryList.getSelectedIndex();
                     System.out.println(entryList.getSelectedValue());
                     //addresses
                     Data d = addresses.getElement(selectedIndex);
                     //Set text fields to current info
                     if(selectedIndex != -1) {
                         lastField.setText(d.getLastName());
                         firstField.setText(d.getFirstName());
                         address1Field.setText(d.getAddress());
                         address2Field.setText(d.getAddressTwo());
                         cityField.setText(d.getCity());
                         stateField.setText(d.getState());
                         zipField.setText(d.getZipCode());
                         phoneField.setText(d.getPhoneNumber());
                         emailField.setText(d.getEmail());
                     }
                 }
           } catch (Exception ex) {
           }
           }
       });
        /* File Options */
        // File > New
        // Open new BookFrame instance
        menuNew.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            // Create and open new address book
            BookFrame frame = new BookFrame("");
          }
        });
        menuOpen.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
              File file = fileChooser.getSelectedFile();
              // TODO: Check if file is correct format?
              File f1 = file.getAbsoluteFile();
              String path = f1.getAbsolutePath();
              BookFrame frame = new BookFrame(path);
            }
          }
        });
        menuClose.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BookFrame.this.dispatchEvent(new WindowEvent(BookFrame.this, WindowEvent.WINDOW_CLOSING));
          }
        });
        menuSave.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BookFrame.this.addresses.saveAddresses();
          }
        });
        menuSaveAs.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BookFrame.this.addresses.saveAddressesAs();
          }
        });
        menuQuit.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            System.exit(0);
          }
        });
        // Event listener for when the JFrame is closed
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
              Main.WINDOW_COUNT--;
              if (Main.WINDOW_COUNT <= 0) {
                System.exit(0);
              }
            }
        });
        // Load file if a file path is provided with Constructor
        if (!path.equals("")) {
          addresses.openAddresses(path);
          ArrayList<Data> a = BookFrame.this.addresses.getArrayList();
          Data d;
          for (int i = 0; i < a.size(); i++) {
            d = a.get(i);
            BookFrame.this.model.addElement(d.formatData());
          }
        }
        //Display the window.
        this.pack();
        this.setVisible(true);
        Main.WINDOW_COUNT++;
    }
    // Checks address entry and populates error log on GUI. Returns true if valid entry, false otherwise.
    private boolean isValidEntry(String l, String f, String a, String a2, String ci, String st, String z, String p, String em) {
      boolean error = false;
      // Check for First or Last name
      if ((l.length() == 0 && f.length() == 0)) {
        errorLabel.setForeground(Color.red);
        errorLabel.setText("First or Last name is required");
        return false;
      }
      // Check for at least one contact field
      if (a.equals("")
        && a2.equals("")
        && ci.equals("")
        && st.equals("")
        && z.equals("")
        && p.equals("")
        && em.equals("")) {
          errorLabel.setForeground(Color.red);
          errorLabel.setText("Error: At least one contact field is required.");
          return false;
        }
      // Check that state is entered in two letter format
      if (st.length() != 0 && st.length() != 2) {
        errorLabel.setForeground(Color.orange);
        errorLabel.setText("Warning: State field should be a two letter abreviation.");
        error = true;
      }
      // Check zip code field
      // First check for format of xxxxx
      if (z.length() != 0 && z.length() != 5) {
        // Next check for xxxxx-xxxxx
        if (z.length() != 11 || !z.contains("-")) {
          errorLabel.setForeground(Color.orange);
          errorLabel.setText("Warning: Zip code should be xxxxx or xxxxx-xxxxx.");
          error = true;
        }
      }
      // Count number of digits in phone number
      int count = 0;
      int len = p.length();
      for (int i = 0; i < len; i++) {
        if (Character.isDigit(p.charAt(i))) {
          count++;
        }
      }
      // Total digits should be either 10 or 11 with country code for U.S. phone number
      if (count != 10 && count != 11) {
        errorLabel.setForeground(Color.orange);
        errorLabel.setText("Warning: U.S. phone number should be 10 or 11 digits.");
        error = true;
      }
      // Check email field
      if (em.length() != 0 && !em.contains("@")) {
        errorLabel.setForeground(Color.orange);
        errorLabel.setText("Warning: Email field should contain '@' symbol.");
        error = true;
      }
      // All verifications passed, add entry and update text area, input fields, and error log
      if (!error) {
        errorLabel.setText("");
      }
      return true;
    }
  // Refreshes the entry list
  public void refreshList(){
    model.clear();
    for(int i =0; i < addresses.size(); i++){
        model.addElement(addresses.getElement(i).formatData());
    }
  }
   }