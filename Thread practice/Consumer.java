import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Consumer implements Runnable {

	private String current_string;
	private Producer _producer;
	private String _name;
	private static LinkedBlockingQueue<String> _queue;
	private static int index;
	private static String _max = "";
	private  int _consumed;
	private Random random = new Random();
	
	
	public Consumer(LinkedBlockingQueue<String> queue, String name, Producer producer){
		_queue = queue;
		_name = name;
		_producer = producer;
	}
	
	@Override
	public void run() {

		//while the producer is running or queue isnt empty
		while(_queue.peek() != null || _producer.isRunning())
		{
			//try to poll the head from the queue
			try {
				current_string = _queue.poll(10, TimeUnit.MILLISECONDS);
				_consumed++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//if the head isn't null, compare it to max string
			if (current_string != null){
				if(_queue.peek().toString().compareTo(_max) >= 0 ){
					_max = _queue.peek().toString();
				}
			}
			
			//get wait time
			long wait_time_ms = random.nextInt(10);
			//wait for up to 10 ms
			try {
				Thread.sleep(wait_time_ms);
			} 	catch (InterruptedException e1) {
					e1.printStackTrace();
			}
	
			
			if(index % 1000 == 0){
				System.out.println(_name + " consumed: " + index);
			}
			
			if(index == 2000000){
				System.out.println(_name + " done consuming! " + _consumed + " consumed");
				System.out.println(_name + " max string: " + _max);
			}
			
			index++;
		
		}
		
	}
	
}
