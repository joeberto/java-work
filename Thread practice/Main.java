import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

	public static void main(String[] args) {


		LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>(100000);
		
		Producer producer = new Producer(queue);
		Consumer consumer1 = new Consumer(queue, "Consumer 1", producer);
		Consumer consumer2 = new Consumer(queue, "Consumer 2", producer);
		
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.submit(producer);
		executor.submit(consumer1);
		executor.submit(consumer2);
		executor.shutdown();
	}

}
