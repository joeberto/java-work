import java.util.concurrent.LinkedBlockingQueue;
import java.util.UUID;

public class Producer implements Runnable {
	
	
	private LinkedBlockingQueue<String> _queue;
	private int index;
	private boolean running = false;
	
	public Producer(LinkedBlockingQueue<String> queue) {
		_queue = queue;
	}
	
	
	public boolean isRunning(){
		if (running){
			return true;		
		}
		return false;
	}
	@Override
	public void run() {
		
		running = true;
		
		for (int i=0; i < 2000000; i++){
			
			if (index % 1000 == 0){
				System.out.println("produced: " + index);
				
			}
			String randomS = UUID.randomUUID().toString();
			index++;
			
			try {
				_queue.put(randomS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		running = false;
	}
	

}
