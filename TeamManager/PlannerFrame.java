import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.MouseEvent;

public class PlannerFrame extends JFrame{

	private JLabel errorLabel;
	private DefaultListModel<Meeting> model;
	private Schedule schedule;
	public ArrayList<Team> teams;

	// For the drag down lists
	private Integer[] year = {2017, 2018, 2019};
	private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	private Integer[] days = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
	private Integer[] hours  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	private String[] minutes = {"00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"};
	private String[] period = {"AM", "PM"};
	private ArrayList<String> available = new ArrayList<String>();
	private String[] sorting = {"Start Time", "End Time", "Team"}; 
    private JComboBox<String> puAvailableList; //this has to be an instance variable for the dropdown to be mutable

	public PlannerFrame(String path, Schedule loginSchedule) {

		// Start of Planner Frame ---------------------------------------------------------------------------
		super("Team Meeting Planner");
		this.setPreferredSize(new Dimension(600, 400));
		this.setResizable(false);
		this.setLayout(new BorderLayout());

		this.schedule = loginSchedule;
		teams = loginSchedule.getTeams();
		available.add("Available");
		for (Team t : teams) {
			available.add(t.getTeamName());
		}

		// Start of Left Panel ------------------------------------------------------------------------------
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BorderLayout());

		// Left Panel Components
		JPanel meetingPanel = new JPanel(new FlowLayout(5, 20, 1));
		model = new DefaultListModel<>();
		JPanel teamPanel = new JPanel(new FlowLayout(5, 30, 1));

		// load file
		schedule.parseScheduleFile(LoginFrame.scheduleFile);
		for (Meeting m : schedule.getMeetings()) {
            if (m.getTeam() != null) {
                if (!available.contains(m.getTeam().getTeamName())) {
                    m.setTeam(null);
                    m.setMeetingName("Available");
                }
            }
			model.addElement(m);
		}

		JList<Meeting> meetingList = new JList<>(model);
		JScrollPane meetingPane = new JScrollPane(meetingList);

		// Set Left Panel Components
		JButton addButton = new JButton("New Meeting");
		JLabel sortingHeader = new JLabel("Sort By");
		JButton newTeamButton = new JButton ("New Team");
		JButton removeTeamButton = new JButton("Remove Team");
		JComboBox<String> sortingList = new JComboBox<String>(sorting);
		addButton.setFocusPainted(false);
		meetingPanel.setSize(200, 100);

		sortMeetings(sortingList);

		meetingPane.setMaximumSize(new Dimension(300, 400));
		meetingPane.setMinimumSize(new Dimension(300, 400));

		// Add Left Panel Components
		meetingPanel.add(addButton);
		meetingPanel.add(sortingHeader);
		meetingPanel.add(sortingList);
		teamPanel.add(newTeamButton);
		teamPanel.add(removeTeamButton);
		leftPanel.add(meetingPane,BorderLayout.CENTER);
		leftPanel.add(meetingPanel,BorderLayout.NORTH); 
		leftPanel.add(teamPanel, BorderLayout.SOUTH);
		// End of Left Panel -----------------------------------------------------------------------------------


		//Start of Right Panel ---------------------------------------------------------------------------------
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));

		//Right Panel Components
		JPanel startTimeHeaderPanel = new JPanel(new FlowLayout(0, 0, 0)); 
		JPanel startMeetingPanel = new JPanel(new FlowLayout(0, 0, 0));
		startMeetingPanel.setBorder(new EmptyBorder(0,0,30,0));
		JPanel endTimeHeaderPanel = new JPanel(new FlowLayout(0, 0 , 0));
		JPanel endMeetingPanel = new JPanel(new FlowLayout(0, 0, 0));
		endMeetingPanel.setBorder(new EmptyBorder(0,0,30,0));
		JPanel availablePanel = new JPanel(new FlowLayout(0 , 0, 0));
		JPanel buttonPanel = new JPanel(new FlowLayout(0, 0, 0));
		buttonPanel.setBorder(new EmptyBorder(30,0,0,0));
		JPanel errorPanel = new JPanel(new FlowLayout(0, 0, 0));


		// Set Right Panel Components
		JLabel startTimeHeader = new JLabel("Start Meeting Time");
		startTimeHeaderPanel.add(startTimeHeader);

		JLabel startTimeLabel = new JLabel("");

		// Add Start Meeting Panel Components
		startMeetingPanel.add(startTimeHeaderPanel);
		startMeetingPanel.add(startTimeLabel);

		JLabel endTimeHeader = new JLabel("End Meeting Time");
		endTimeHeaderPanel.add(endTimeHeader);

		JLabel endTimeLabel = new JLabel("");
		endMeetingPanel.add(endTimeLabel);

		JLabel availableLabel = new JLabel("");
		availablePanel.add(availableLabel);


		// Add End Meeting Panel Components
		endMeetingPanel.add(endTimeHeaderPanel);

		JButton editButton = new JButton("Edit");
		JButton deleteButton = new JButton("Delete");
		buttonPanel.add(editButton);
		buttonPanel.add(deleteButton);

		errorLabel = new JLabel("");
		errorPanel.add(errorLabel);

		// Add Components to Right Panel
		rightPanel.add(startTimeHeaderPanel);
		rightPanel.add(startMeetingPanel);
		rightPanel.add(endTimeHeaderPanel);
		rightPanel.add(endMeetingPanel);
		rightPanel.add(availablePanel);
		rightPanel.add(buttonPanel);
		rightPanel.add(errorPanel);

		startTimeHeaderPanel.setVisible(false);
		startMeetingPanel.setVisible(false);
		endTimeHeaderPanel.setVisible(false);
		endMeetingPanel.setVisible(false);
		availablePanel.setVisible(false);
		buttonPanel.setVisible(false);
		errorPanel.setVisible(false);
		// End of Right Panel----------------------------------------------------------------------------------


		//Add left and right panels to Planner Frame
		JSplitPane splitPane = new JSplitPane();
		splitPane.setLeftComponent(leftPanel);
		splitPane.setRightComponent(rightPanel);
		splitPane.setEnabled(false);

		//Set up main Frame
		this.add(splitPane);	
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true); 

		// End of Planner Frame -------------------------------------------------------------------------------


		//Start Pop up for New Entry --------------------------------------------------------------------------
		JSplitPane puMeetingPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

		//Pop Up Meeting Panel Components
		JSplitPane puSetMeetingPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JPanel puReservedPanel = new JPanel(new GridLayout(0,1));

		// Pop Up SetMeeting Components
		JPanel puStartMeetingPanel = new JPanel();
		puStartMeetingPanel.setLayout(new BoxLayout(puStartMeetingPanel, BoxLayout.Y_AXIS));
		JPanel puEndMeetingPanel = new JPanel();
		puEndMeetingPanel.setLayout(new BoxLayout(puEndMeetingPanel, BoxLayout.Y_AXIS));

		// Pop Up Start Meeting Panel Components
		JPanel puStartHeaderPanel = new JPanel(new FlowLayout(5,20,10));
		JPanel puStartDropDownHeaderPanel = new JPanel(new GridLayout(1,6));
		JPanel puStartDropDownBoxes = new JPanel(new GridLayout(1,6));

		// Set Pop Up Start Meeting Components
		JLabel puStartHeader = new JLabel("Start Meeting Time");
		puStartHeaderPanel.add(puStartHeader);

		JLabel pusYearHeader = new JLabel("Year");
		JLabel pusMonthHeader = new JLabel("Month");
		JLabel pusDayHeader = new JLabel("Day");
		JLabel pusHourHeader = new JLabel("Hour"); 
		JLabel pusMinuteHeader = new JLabel("Minute");
		JLabel pusPeriodHeader = new JLabel("Period");
		puStartDropDownHeaderPanel.add(pusYearHeader);
		puStartDropDownHeaderPanel.add(pusMonthHeader);
		puStartDropDownHeaderPanel.add(pusDayHeader);
		puStartDropDownHeaderPanel.add(pusHourHeader);
		puStartDropDownHeaderPanel.add(pusMinuteHeader);
		puStartDropDownHeaderPanel.add(pusPeriodHeader);

		JComboBox<Integer> pusYearList = new JComboBox<Integer>(year);
		JComboBox<String> pusMonthList = new JComboBox<String>(months);
		JComboBox<Integer> pusDayList = new JComboBox<Integer>(days);
		JComboBox<Integer> pusHourList = new JComboBox<Integer>(hours);
		JComboBox<String> pusMinuteList = new JComboBox<String>(minutes);
		JComboBox<String> pusPeriodList = new JComboBox<String>(period);
		puStartDropDownBoxes.add(pusYearList);
		puStartDropDownBoxes.add(pusMonthList);
		puStartDropDownBoxes.add(pusDayList);
		puStartDropDownBoxes.add(pusHourList);
		puStartDropDownBoxes.add(pusMinuteList);
		puStartDropDownBoxes.add(pusPeriodList);

		// Add Pop Up Start Meeting Panel Components
		puStartMeetingPanel.add(puStartHeaderPanel);
		puStartMeetingPanel.add(puStartDropDownHeaderPanel);
		puStartMeetingPanel.add(puStartDropDownBoxes);

		// Pop Up End Meeting Panel Components
		JPanel puEndHeaderPanel = new JPanel(new FlowLayout(5,20,10));
		JPanel puEndDropDownHeaderPanel = new JPanel(new GridLayout(1,6));
		JPanel puEndDropDownBoxes = new JPanel(new GridLayout(1,6));

		// Set Pop Up End Meeting Components
		JLabel puEndHeader = new JLabel("End Meeting Time");
		puEndHeaderPanel.add(puEndHeader);

		JLabel pueYearHeader = new JLabel("Year");
		JLabel pueMonthHeader = new JLabel("Month");
		JLabel pueDayHeader = new JLabel("Day");
		JLabel pueHourHeader = new JLabel("Hour"); 
		JLabel pueMinuteHeader = new JLabel("Minute");
		JLabel puePeriodHeader = new JLabel("Period");
		puEndDropDownHeaderPanel.add(pueYearHeader);
		puEndDropDownHeaderPanel.add(pueMonthHeader);
		puEndDropDownHeaderPanel.add(pueDayHeader);
		puEndDropDownHeaderPanel.add(pueHourHeader);
		puEndDropDownHeaderPanel.add(pueMinuteHeader);
		puEndDropDownHeaderPanel.add(puePeriodHeader);

		JComboBox<Integer> pueYearList = new JComboBox<Integer>(year);
		JComboBox<String> pueMonthList = new JComboBox<String>(months);
		JComboBox<Integer> pueDayList = new JComboBox<Integer>(days);
		JComboBox<Integer> pueHourList = new JComboBox<Integer>(hours);
		JComboBox<String> pueMinuteList = new JComboBox<String>(minutes);
		JComboBox<String> puePeriodList = new JComboBox<String>(period);
		puEndDropDownBoxes.add(pueYearList);
		puEndDropDownBoxes.add(pueMonthList);
		puEndDropDownBoxes.add(pueDayList);
		puEndDropDownBoxes.add(pueHourList);
		puEndDropDownBoxes.add(pueMinuteList);
		puEndDropDownBoxes.add(puePeriodList);

		// Add Pop Up End Meeting Components
		puEndMeetingPanel.add(puEndHeaderPanel);
		puEndMeetingPanel.add(puEndDropDownHeaderPanel);
		puEndMeetingPanel.add(puEndDropDownBoxes);

		// Set Pop up Reserved Panel
	    puAvailableList = new JComboBox<String>(available.toArray(new String[available.size()]));
		puReservedPanel.add(puAvailableList);

		// Add Pop Up Set Meeting Panel Components
		puSetMeetingPanel.setTopComponent(puStartMeetingPanel);
		puSetMeetingPanel.setBottomComponent(puEndMeetingPanel);

		// Add Pop Up Meeting Panel Components
		puMeetingPanel.setTopComponent(puSetMeetingPanel);
		puMeetingPanel.setBottomComponent(puReservedPanel);
		//End of Pop Up for New Entry ----------------------------------------------------------
		
		// Start Pop Up New Team Components
		JPanel puNewTeamPanel = new JPanel();
		puNewTeamPanel.setLayout(new BoxLayout(puNewTeamPanel, BoxLayout.Y_AXIS));
		
		JLabel puNewTeamHeader = new JLabel("Create a New Team");
		JPanel puNewTeamInfoPanel = new JPanel();
		puNewTeamInfoPanel.setLayout(new BoxLayout(puNewTeamInfoPanel, BoxLayout.Y_AXIS));
		JLabel puNewTeamNameLabel = new JLabel("Team Name");
		JTextField puNewTeamName = new JTextField();
		JLabel puNewTeamPasswordLabel = new JLabel("Team Password");
		JPasswordField puNewTeamPassword = new JPasswordField();
		JLabel puNewTeamRetypeLabel = new JLabel("Retype Password");
		JPasswordField puNewTeamRetype = new JPasswordField();
		
		puNewTeamInfoPanel.add(puNewTeamNameLabel);
		puNewTeamInfoPanel.add(puNewTeamName);
		puNewTeamInfoPanel.add(puNewTeamPasswordLabel);
		puNewTeamInfoPanel.add(puNewTeamPassword);
		puNewTeamInfoPanel.add(puNewTeamRetypeLabel);
		puNewTeamInfoPanel.add(puNewTeamRetype);
		
		// Set Pop Up New Team Components
		puNewTeamHeader.setBorder(new EmptyBorder(10,60,30,0));
		puNewTeamInfoPanel.setBorder(new EmptyBorder(0,0,30,0));
		
		// Add Pop Up New Team Components
		puNewTeamPanel.add(puNewTeamHeader);
		puNewTeamPanel.add(puNewTeamInfoPanel);
		// End Pop Up New Team Components 
		
		
		// Start Pop up Remove Team Components
		JPanel puRemoveTeamPanel = new JPanel(); 
		puRemoveTeamPanel.setLayout(new BoxLayout(puRemoveTeamPanel, BoxLayout.Y_AXIS));
		
		JLabel puRemoveTeamHeader = new JLabel("Select a Team to Remove");
		JComboBox<String> puRemoveTeamList = new JComboBox<String>(available.toArray(new String[available.size()]));
		puRemoveTeamList.removeItem("Available");
		
		// Set Remove Team Components
		puRemoveTeamHeader.setBorder(new EmptyBorder(10,0,20,0));
		
		// Add Remove Team Components
		puRemoveTeamPanel.add(puRemoveTeamHeader);
		puRemoveTeamPanel.add(puRemoveTeamList);
		// End Remove Team
		
		// Sort Action Listener-----------------------------------------------------------------
		sortingList.addItemListener(new java.awt.event.ItemListener(){
			@Override
			public void itemStateChanged(java.awt.event.ItemEvent e){
				if (e.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
					sortMeetings(sortingList);
				}
			}
		});

		
		// List Action Listener-----------------------------------------------------------------
		meetingList.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {

				try{
					Meeting selectedMeeting = meetingList.getSelectedValue();

					startTimeLabel.setText(selectedMeeting.getStartMonth() + " " + selectedMeeting.getStartDay() + " " 
							+ selectedMeeting.getStartYear() + " at " + selectedMeeting.getStartHour() + ":" + selectedMeeting.getStartMinute() + " "
							+ selectedMeeting.getStartPeriod());
					endTimeLabel.setText(selectedMeeting.getEndMonth() + " " + selectedMeeting.getEndDay() + " " 
							+ selectedMeeting.getEndYear() + " at " + selectedMeeting.getEndHour() + ":" + selectedMeeting.getEndMinute() + " "
							+ selectedMeeting.getEndPeriod());

					if(selectedMeeting.getTeam() != null){
						availableLabel.setText(selectedMeeting.getTeam().getTeamName());
					}
					else{
						availableLabel.setText("Available");
					}

					startTimeHeaderPanel.setVisible(true);
					startMeetingPanel.setVisible(true);
					endTimeHeaderPanel.setVisible(true);
					endMeetingPanel.setVisible(true);
					availablePanel.setVisible(true);
					buttonPanel.setVisible(true);
					errorPanel.setVisible(true);

				} catch(Exception ex){
				}

			}

		});



		//Button Action Listeners----------------------------------------------------------------
		addButton.addActionListener(new java.awt.event.ActionListener(){
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e){

				//Set current time
				int cYear = Meeting.getCurrentYear();
				String cMonth = Meeting.getCurrentMonth();
				int cDay = Meeting.getCurrentDay();
				int cHour = Meeting.getCurrentHour();
				String cMinute = Meeting.getCurrentMinute();
				String cPeriod = Meeting.getCurrentPeriod();

                int nYear = Meeting.getNextYear();
				String nMonth = Meeting.getNextMonth();
				int nDay = Meeting.getNextDay();
				int nHour = Meeting.getNextHour();
				String nMinute = Meeting.getNextMinute();
				String nPeriod = Meeting.getNextPeriod();

				pusYearList.setSelectedItem(cYear);
				pusMonthList.setSelectedItem(cMonth);
				pusDayList.setSelectedItem(cDay);
				pusHourList.setSelectedItem(cHour);
				pusMinuteList.setSelectedItem(cMinute);
				pusPeriodList.setSelectedItem(cPeriod);

				pueYearList.setSelectedItem(nYear);
				pueMonthList.setSelectedItem(nMonth);
				pueDayList.setSelectedItem(nDay);
				pueHourList.setSelectedItem(nHour);
				pueMinuteList.setSelectedItem(nMinute);
				puePeriodList.setSelectedItem(nPeriod);

				puAvailableList.setSelectedItem("Available");

				int result = JOptionPane.showConfirmDialog(null, puMeetingPanel, "Meeting Time", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

				if(result == JOptionPane.OK_OPTION){

					int sHour;
					int eHour;

					if(((String)pusPeriodList.getSelectedItem()).compareTo("AM")== 0){
						sHour = (Integer)pusHourList.getSelectedItem() % 12;
					}
					else{
						sHour = ((Integer)pusHourList.getSelectedItem() % 12) + 12; 
					}

					if(((String)puePeriodList.getSelectedItem()).compareTo("AM")== 0){
						eHour = (Integer)pueHourList.getSelectedItem() % 12;
					}
					else{
						eHour = ((Integer)pueHourList.getSelectedItem() % 12) + 12;
					}

					LocalDateTime startTime = LocalDateTime.of((Integer)pusYearList.getSelectedItem(), 
							(Integer)pusMonthList.getSelectedIndex() + 1, 
							(Integer)pusDayList.getSelectedItem(), 
							sHour, 
							Integer.parseInt((String)pusMinuteList.getSelectedItem()));

					LocalDateTime endTime = LocalDateTime.of((Integer)pueYearList.getSelectedItem(), 
							(Integer)pueMonthList.getSelectedIndex() + 1, 
							(Integer)pueDayList.getSelectedItem(), 
							eHour, 
							Integer.parseInt((String)pueMinuteList.getSelectedItem()));

					Meeting meeting = new Meeting(startTime, endTime);
					
					if (Meeting.isValid(meeting) && meeting.getStartTime().compareTo(LocalDateTime.now().plusMinutes(-5))>0) {
                        boolean overlap = false;
                        for (Meeting m : schedule.getMeetings()) {
                            if ((meeting.getEndTime().compareTo(m.getStartTime()) > 0)&&(meeting.getStartTime().compareTo(m.getEndTime()) < 0)) { // if start is before another meetings end and end is after another meetings start
                                overlap = true;
                            }
                        }
                        if (!overlap) {
                            meeting.setMeetingName((String)puAvailableList.getSelectedItem());		
                            for (Team t : teams) {
                                if (t.getTeamName().compareTo((String)puAvailableList.getSelectedItem()) == 0) {
                                    meeting.setTeam(t); 
                                }
                            }
                            model.addElement(meeting);
                            schedule.addMeeting(meeting);
                            sortMeetings(sortingList);
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "Meeting overlap. Meeting was not created");
                        }
					}
					else{
						JOptionPane.showMessageDialog(null, "Invalid meeting time. Meeting was not created");
					}

				}
				else{
					System.out.println("User canceled operation");
				}
			}

		});

		
		editButton.addActionListener(new java.awt.event.ActionListener(){
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e){

				if(!meetingList.isSelectionEmpty()){
					//Set current time
					Meeting oldMeeting = meetingList.getSelectedValue();

					pusYearList.setSelectedItem(oldMeeting.getStartYear());
					pusMonthList.setSelectedItem(oldMeeting.getStartMonth());
					pusDayList.setSelectedItem(oldMeeting.getStartDay());
					pusHourList.setSelectedItem(oldMeeting.getStartHour());
					pusMinuteList.setSelectedItem(oldMeeting.getStartMinute());
					pusPeriodList.setSelectedItem(oldMeeting.getStartPeriod());

					pueYearList.setSelectedItem(oldMeeting.getEndYear());
					pueMonthList.setSelectedItem(oldMeeting.getEndMonth());
					pueDayList.setSelectedItem(oldMeeting.getEndDay());
					pueHourList.setSelectedItem(oldMeeting.getEndHour());
					pueMinuteList.setSelectedItem(oldMeeting.getEndMinute());
					puePeriodList.setSelectedItem(oldMeeting.getEndPeriod());

					puAvailableList.setSelectedItem(oldMeeting.getMeetingName());

					int result = JOptionPane.showConfirmDialog(null, puMeetingPanel, "Meeting Time", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

					if(result == JOptionPane.OK_OPTION){

						int sHour;
						int eHour;

						if(((String)pusPeriodList.getSelectedItem()).compareTo("AM")== 0){
							sHour = (Integer)pusHourList.getSelectedItem() % 12;
						}
						else{
							sHour = ((Integer)pusHourList.getSelectedItem() % 12) + 12; 
						}

						if(((String)puePeriodList.getSelectedItem()).compareTo("AM")== 0){
							eHour = (Integer)pueHourList.getSelectedItem() % 12;
						}
						else{
							eHour = ((Integer)pueHourList.getSelectedItem() % 12) + 12;
						}

						LocalDateTime startTime = LocalDateTime.of((Integer)pusYearList.getSelectedItem(), 
								(Integer)pusMonthList.getSelectedIndex() + 1, 
								(Integer)pusDayList.getSelectedItem(), 
								sHour, 
								Integer.parseInt((String)pusMinuteList.getSelectedItem()));

						LocalDateTime endTime = LocalDateTime.of((Integer)pueYearList.getSelectedItem(), 
								(Integer)pueMonthList.getSelectedIndex() + 1, 
								(Integer)pueDayList.getSelectedItem(), 
								eHour, 
								Integer.parseInt((String)pueMinuteList.getSelectedItem()));

						Meeting newMeeting = new Meeting(startTime, endTime);
						if (Meeting.isValid(newMeeting) && newMeeting.getStartTime().compareTo(LocalDateTime.now().plusMinutes(-5))>0) {

							model.removeElement(oldMeeting);
							schedule.removeMeeting(oldMeeting);
							model.addElement(newMeeting);
							schedule.addMeeting(newMeeting);
							sortMeetings(sortingList);

							newMeeting.setMeetingName((String)puAvailableList.getSelectedItem());		
							for (Team t : teams) {
								if (t.getTeamName().compareTo((String)puAvailableList.getSelectedItem()) == 0) {
									newMeeting.setTeam(t); 
								}
							}
							meetingList.setSelectedValue(newMeeting, true);
							startTimeLabel.setText(newMeeting.getStartMonth() + " " + newMeeting.getStartDay() + " " 
									+ newMeeting.getStartYear() + " at " + newMeeting.getStartHour() + ":" + newMeeting.getStartMinute() + " "
									+ newMeeting.getStartPeriod());
							endTimeLabel.setText(newMeeting.getEndMonth() + " " + newMeeting.getEndDay() + " " 
									+ newMeeting.getEndYear() + " at " + newMeeting.getEndHour() + ":" + newMeeting.getEndMinute() + " "
									+ newMeeting.getEndPeriod());
							if(newMeeting.getTeam() != null){
						        availableLabel.setText(newMeeting.getTeam().getTeamName());
							}
							else{
								availableLabel.setText("Available");
							}

						}
						else{
							JOptionPane.showMessageDialog(null,"Invalid meeting time. Meeting was not created");
						}

					}
					else{
						System.out.println("User canceled operation");
					}
				}
			}
		});

		
		deleteButton.addActionListener(new java.awt.event.ActionListener(){
			Object[] options = {"Delete", "Cancel"};
			
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e){
				int result = JOptionPane.showOptionDialog(null, "Are you sure you want to delete this meeting?","Delete Meeting", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, null);
				if(result == JOptionPane.OK_OPTION){
					Meeting selectedMeeting = meetingList.getSelectedValue();
					model.removeElement(selectedMeeting);
					schedule.removeMeeting(selectedMeeting);

					startTimeHeaderPanel.setVisible(false);
					startMeetingPanel.setVisible(false);
					endTimeHeaderPanel.setVisible(false);
					endMeetingPanel.setVisible(false);
					availablePanel.setVisible(false);
					buttonPanel.setVisible(false);
					errorPanel.setVisible(false);
				}
			}
		});
	
		
		newTeamButton.addActionListener(new java.awt.event.ActionListener(){
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e){
				int result = JOptionPane.showConfirmDialog(null, puNewTeamPanel, "New Team", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				
                boolean failure = false;
				if(result == JOptionPane.OK_OPTION){
					String teamName = puNewTeamName.getText();
					String password = String.valueOf(puNewTeamPassword.getPassword());
					String retype = String.valueOf(puNewTeamRetype.getPassword());

					boolean dup = false;

					if(teamName.length() != 0){

						for (String tn : available) {
							if (tn.compareTo(teamName) == 0) {
								dup = true;
							}
						}
						if (!dup) {
							if (password.length() != 0) {
								if(password.compareTo(retype) != 0){
									JOptionPane.showMessageDialog(null, "Passwords do not match");
                                    failure = true;
								}
								else{
									Team newTeam = new Team(teamName, Team.md5String(password));
									teams.add(newTeam);
									available.add(newTeam.getTeamName());
									puAvailableList.addItem(newTeam.getTeamName());
									puRemoveTeamList.addItem(newTeam.getTeamName());
								}
							}
							else {
								JOptionPane.showMessageDialog(null, "Please supply a password");
                                failure = true;
							}
						}
						else {
							JOptionPane.showMessageDialog(null, "Team with this name already exists");
                            failure = true;
						}

						puNewTeamName.setText("");
						puNewTeamPassword.setText("");
						puNewTeamRetype.setText("");

					}
					else{
						JOptionPane.showMessageDialog(null, "Please supply a team name");
                        failure = true;
					}
				}
                if (failure == true) {
                    newTeamButton.doClick();
                }
			}
		});

		
		removeTeamButton.addActionListener(new java.awt.event.ActionListener(){
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e){
				int result = JOptionPane.showConfirmDialog(null, puRemoveTeamPanel, "Remove Team", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				
				if(result == JOptionPane.OK_OPTION){
					String removeTeamName = (String) puRemoveTeamList.getSelectedItem();
                    Team removeTeam = new Team(removeTeamName);
                    Team toRemove = null;
					
					available.remove(removeTeamName);
					puAvailableList.removeItem(removeTeamName);
					puRemoveTeamList.removeItem(removeTeamName);

                    for (Team rt : teams) {
                        if (Team.NameComparator.compare(rt, removeTeam) == 0) {
                            toRemove = rt;
                        }
                    }
                    teams.remove(toRemove);
                    for (Meeting m : schedule.getMeetings()) {
                        if (Team.NameComparator.compare(m.getTeam(), removeTeam) == 0) {
                            m.setMeetingName("Available");
                            m.setTeam(null);
                        }
                    }
					sortMeetings(sortingList);
					
					//updates Gui if team was removed while their meeting was selected
					if(availableLabel.getText().compareTo(removeTeamName) == 0){
						availableLabel.setText("Available");
					}
				}
			}
		});
		
		
		// Custom Close Operation
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				// save the new schedule to the file
				schedule.setTeams(teams);
				schedule.saveScheduleFile(LoginFrame.scheduleFile);
				schedule.saveTeamFile(LoginFrame.teamsFile);
				System.exit(0);
			}
		});

	}

	public void sortMeetings(JComboBox<String> sortingList) {
		ArrayList<Meeting> meets = schedule.getMeetings();
		switch (sortingList.getSelectedIndex()) {
		case 0:
			Collections.sort(meets, Meeting.StartTimeComparator);
			break;
		case 1:
			Collections.sort(meets, Meeting.EndTimeComparator);
			break;
		case 2:
			Collections.sort(meets, Meeting.TeamComparator);
			break;
		case 3:
			Collections.sort(meets, Meeting.NameComparator);
			break;
		}

		model.removeAllElements();
		for (int i = 0; i < meets.size(); i++) {
			model.addElement(meets.get(i));
		}
	}

}

