import java.util.*;
import java.io.*;

public class Schedule {
    private ArrayList<Meeting> meetings;
    private ArrayList<Team> teams;

    public Schedule() {
        this.meetings = new ArrayList<Meeting>();
        this.teams = new ArrayList<Team>();
    }
    
    public ArrayList<Meeting> getMeetings() {
        return this.meetings;
    }
 
    public ArrayList<Team> getTeams() {
        return this.teams;
    }   

    public void setMeetings(ArrayList<Meeting> newMeetings) {
        this.meetings = newMeetings;
    }

    public void setTeams(ArrayList<Team> newTeams) {
        this.teams = newTeams;
    }

    public void addMeeting(Meeting m){
    	meetings.add(m);
    }
    
    public boolean removeMeeting(Meeting m){
    	return meetings.remove(m);
    }
    
    public boolean addTeam(String teamName, String teamPassword) {
        Team t = new Team(teamName, teamPassword);
        teams.add(t);
        return true;
    }

    public boolean removeTeam(String teamName) {
        for (Team t : teams) {
            if (t.getTeamName() == teamName) {
                teams.remove(t);
                return true;
            }
        }
        return false;
    }

    // Parse a saved schedule and add each meeting to meetings
    public boolean parseScheduleFile(String path) {
        File file = new File(path);
        try {
            Scanner scan = new Scanner(file);
            String line = "";
            Meeting m;
            while (scan.hasNext()) {
                line = scan.nextLine();
                // count number of '|' characters
                int count = line.length() - line.replace("|", "").length();
                if (count != 4) {
                    return false;
                }
                m = new Meeting(line);
                if (Meeting.isValid(m)) {
                    boolean overlap = false;
                    for (Meeting me : meetings) {
                        if ((m.getEndTime().compareTo(me.getStartTime()) > 0)&&(m.getStartTime().compareTo(me.getEndTime()) < 0)) { // if start is before another meetings end and end is after another meetings start
                            overlap = true;
                        }
                    }
                    if (!overlap) {
                        meetings.add(m);
                    }
                }
            }
            scan.close();
        } catch (Exception e) {

        }
        return true;
    }
    public boolean saveScheduleFile(String path) {
        try {
            FileWriter fw = new FileWriter(path, false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            for (Meeting m : meetings) {
                pw.print(m.fileString());
            }
            pw.close();
            return true;
        } catch (IOException e) {return false;}
    }
 
    // Parse a saved team file and add each team to teams
    public boolean parseTeamFile(String path) {
        File file = new File(path);
        try {
            Scanner scan = new Scanner(file);
            String line = "";
            Team t;
            while (scan.hasNext()) {
                line = scan.nextLine();
                // count number of '|' characters
                int count = line.length() - line.replace("|", "").length();
                if (count != 2) {
                    return false;
                }
                String[] parts = line.split("\\|");
                t = new Team(parts[0], parts[1]);
                teams.add(t);
            }
            scan.close();
        } catch (Exception e) {

        }
        return true;
    }
    public boolean saveTeamFile(String path) {
        try {
            FileWriter fw = new FileWriter(path, false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            for (Team t : teams) {
                pw.print(t.fileString());
            }
            pw.close();
            return true;
        } catch (IOException e) {return false;}
    }       
    
    public boolean authenticateTeam(Team t) {
        for (Team t2 : teams) {
            if (Team.NameComparator.compare(t, t2) == 0) {
                if (Team.PasswordComparator.compare(t, t2) == 0) {
                    return true;
                }
            }
        }
        return false;
    }
}
