import java.time.LocalDateTime;
import java.util.Comparator;

public class Meeting {

    private static String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private static String[] minutes = {"00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"};
    private static String[] period = {"AM", "PM"};

    private String meetingName = "Available";
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Team t = null;
    /*
    FOR FUTURE USE:NOT IMPLEMENTED
    public int type = 0;
        type should be one of the following values:
        0: personal (this should be used for anything that is not one of the next three. Used to signify general busyness, sleep, life, etc.)
        1: team meeting
        2: class
        3: work
    */

    // this is a constructor to take a read line representing a saved meeting and create a Meeting object from it
    public Meeting(Meeting m) {
        this.meetingName = m.getMeetingName();
        this.startTime = m.getStartTime();
        this.endTime = m.getEndTime();
        this.t = m.getTeam();
    }

    public Meeting(String line) {
        String[] parts = line.split("\\|");
        LocalDateTime toStart = LocalDateTime.parse(parts[1]);
        LocalDateTime toEnd = LocalDateTime.parse(parts[2]);
        Team tm = null;
        if (parts[3].compareTo("") != 0) {
            tm = new Team(parts[3]);
        }
        this.meetingName = parts[0];
        this.startTime = toStart;
        this.endTime = toEnd;
        this.t = tm;
    }
    
    public Meeting(LocalDateTime startTime, LocalDateTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Meeting(LocalDateTime startTime, LocalDateTime endTime, Team t) {
        this.meetingName = t.getTeamName();
        this.startTime = startTime;
        this.endTime = endTime;
        this.t = t;
    }
    
    @Override
    public String toString() {
        String s = this.getStartMonth() + " " + this.getStartDay() + " " + this.getStartYear() + " " + this.getStartHour() + ":" + this.getStartMinute() + this.getStartPeriod();
        s += " - ";
        s += this.getEndMonth() + " " + this.getEndDay() + " " + this.getEndYear() + " " + this.getEndHour() + ":" + this.getEndMinute() + this.getEndPeriod();
        s += " : " + this.meetingName;
        return s;
    }
    
    public String getMeetingName() {
        return this.meetingName;
    }

    public LocalDateTime getStartTime() {
    	return this.startTime;
    }
    
    public LocalDateTime getEndTime() {
    	return this.endTime;
    }
    
    public Team getTeam() {
        return this.t;
    }    
    
    public int getStartYear() {
        return this.getStartTime().getYear();
    }
    
    public int getEndYear() {
        return this.getEndTime().getYear();
    }
    
    public static int getCurrentYear(){
    	return LocalDateTime.now().getYear();
    }

     public static int getNextYear(){
    	return LocalDateTime.now().plusMinutes(5).getYear();
    }

    public String getStartMonth() {
        int m = this.getStartTime().getMonthValue();
        return months[m-1];
    }

    public String getEndMonth() {
        int m = this.getEndTime().getMonthValue();
        return months[m-1];
    }
    
    public static String getCurrentMonth(){
    	int m = LocalDateTime.now().getMonthValue();
    	return months[m-1];
    }
    
    public static String getNextMonth(){
    	int m = LocalDateTime.now().plusMinutes(5).getMonthValue();
    	return months[m-1];
    }
    
    public int getStartDay() {
        return this.getStartTime().getDayOfMonth();
    }
    
    public int getEndDay() {
        return this.getEndTime().getDayOfMonth();
    }
    
    public static int getCurrentDay(){
    	return LocalDateTime.now().getDayOfMonth();
    }
    
    public static int getNextDay(){
    	return LocalDateTime.now().plusMinutes(5).getDayOfMonth();
    }
    
    public int getStartHour() {
        return ((this.getStartTime().getHour()+11) % 12)+1;
    }

    public int getEndHour() {
        return ((this.getEndTime().getHour()+11) % 12)+1;
    }
    
    public static int getCurrentHour(){
    	return ((LocalDateTime.now().getHour()+11) % 12)+1;
    }
 
    public static int getNextHour(){
    	return ((LocalDateTime.now().plusMinutes(5).getHour()+11) % 12)+1;
    }

    public String getStartMinute() {
        int m = this.getStartTime().getMinute() / 5;
        return minutes[m];
    }

    public String getEndMinute() {
        int m = this.getEndTime().getMinute() / 5;
        return minutes[m];
    }
    
    public static String getCurrentMinute(){
    	int m = LocalDateTime.now().getMinute() / 5;
    	return minutes[m];
    }
    
    public static String getNextMinute(){
    	int m = LocalDateTime.now().plusMinutes(5).getMinute() / 5;
    	return minutes[m];
    }

    public String getStartPeriod() {
        if (this.getStartTime().getHour() >= 12) {
            return period[1];
        }
        return period[0];
    }

    public String getEndPeriod() {
        if (this.getEndTime().getHour() >= 12) {
            return period[1];
        }
        return period[0];
    }

    public static String getCurrentPeriod(){
    	if(LocalDateTime.now().getHour() >= 12){
    		return period[1];
    	}
    	return period[0];
    }

    public static String getNextPeriod(){
    	if(LocalDateTime.now().getHour() >= 12){
    		return period[1];
    	}
    	return period[0];
    }

    public void setMeetingName(String newName) {
        this.meetingName = newName;
    }

    public void setStartTime(LocalDateTime newStart) {
        this.startTime = newStart;
    }

    public void setEndTime(LocalDateTime newEnd) {
        this.endTime = newEnd;
    }

    public void setTeam(Team t) {
        this.t = t;
    }

    public boolean unreserve(Team t) {
        if (Team.NameComparator.compare(t, this.t) != 0) {
            return false;
        }
        this.setMeetingName("Available");
        this.setTeam(null);
        return true;
    }

    public static boolean isValid(Meeting m) {
        if (m.getEndTime().compareTo(m.getStartTime()) <= 0) {
            return false;
        }
        return true; 
    }

    public static Comparator<Meeting> StartTimeComparator = new Comparator<Meeting>() {
        public int compare(Meeting m1, Meeting m2) {
            return m1.getStartTime().compareTo(m2.getStartTime());
        }
    };

    public static Comparator<Meeting> EndTimeComparator = new Comparator<Meeting>() {
        public int compare(Meeting m1, Meeting m2) {
            return m1.getEndTime().compareTo(m2.getEndTime());
        }
    };

    public static Comparator<Meeting> TeamComparator = new Comparator<Meeting>() {
        public int compare(Meeting m1, Meeting m2) {
            Team t1 = m1.getTeam();
            Team t2 = m2.getTeam();
            if (t1 != null) {
                if (t2 != null) {
                    int nameCheck = t1.getTeamName().compareTo(t2.getTeamName());
                    if (nameCheck == 0) {
                        return m1.getStartTime().compareTo(m2.getStartTime());
                    };
                    return nameCheck;
                }
                return 1;
            }
            if (t2 != null) {
                return -1;
            }
            return 0;
        }
    };

    public static Comparator<Meeting> NameComparator = new Comparator<Meeting>() {
        public int compare(Meeting m1, Meeting m2) {
            int nameCheck = m1.getMeetingName().compareTo(m2.getMeetingName());
            if (nameCheck == 0) {
                return m1.getStartTime().compareTo(m2.getStartTime());
            };
            return nameCheck;
        }
    };

    public String fileString() {
        String s = this.meetingName + "|" + this.startTime.toString() + "|" + this.endTime.toString() + "|";
        if (this.t != null) {
            s += this.t.toString();
        }
        s += "|end\r\n";
        return s;
    }

    
}
