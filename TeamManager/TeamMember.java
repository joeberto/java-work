public class TeamMember {
    
    private String firstName;
    private String lastName;

    public TeamMember(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }   

    public String getFirstName() {
        return this.firstName;
    }   

    public String getLastName() {
        return this.lastName;
    }

    public void setFirstName(String newFirst) {
        this.firstName = newFirst;
    }

    public void setLastName(String newLast) {
        this.lastName = newLast;
    }

    @Override
    public String toString() {
        String s = lastName + ", " + firstName;
        return s;
    }
}
