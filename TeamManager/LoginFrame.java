import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class LoginFrame extends JFrame{
    
    public static final String teamsFile = "./teams.txt";
    public static final String scheduleFile = "./schedule.txt";

    public static final String instructorUsername = "instructor"; 
    public static final String instructorPassword = "175cca0310b93021a7d3cfb3e4877ab6"; // "instructor"

    public Schedule loginSchedule = new Schedule();

	public LoginFrame(){
		
		super("Login");
		this.setPreferredSize(new Dimension(250,250));
		this.setResizable(false);
		this.setLayout(new BorderLayout());

         // load teams file
        loginSchedule.parseTeamFile(teamsFile);
		
		//Login Panel
		JPanel loginPanel = new JPanel();
		loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.Y_AXIS));
		
		//Login Panel Components
		JLabel loginHeader = new JLabel("Team Meeting Planner");
		loginHeader.setBorder(new EmptyBorder(20,20,20,20));
		
		JPanel userinfoPanel = new JPanel();
		userinfoPanel.setLayout(new BoxLayout(userinfoPanel, BoxLayout.Y_AXIS));
		userinfoPanel.setBorder(new EmptyBorder(0,0,15,0));
		JLabel usernameHeader = new JLabel("Username");
		JTextField username = new JTextField();
		JLabel passwordHeader = new JLabel("Password");
		JPasswordField password = new JPasswordField();
		
		
		userinfoPanel.add(usernameHeader);
		userinfoPanel.add(username);
		userinfoPanel.add(passwordHeader);
		userinfoPanel.add(password);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		JButton loginButton = new JButton("Login");
		buttonPanel.add(loginButton);
		
		
		//Set Login Components
		loginHeader.setBorder(new EmptyBorder(20,25,20,20));
		
		loginPanel.add(loginHeader);
		loginPanel.add(userinfoPanel);
		loginPanel.add(buttonPanel);
		
		
		this.add(loginPanel);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//LoginButton Action Listener;
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new java.awt.event.WindowAdapter() {
             @Override
             public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                  System.exit(0);
             }
        });

		loginButton.addActionListener(new java.awt.event.ActionListener(){
			@Override
			public void actionPerformed(java.awt.event.ActionEvent E){
				
                Team toAuth;
				String user = username.getText();
                String pass = String.valueOf(password.getPassword());
                if (pass.length() != 0) {
                    toAuth = new Team(user, Team.md5String(pass));
                }
                else {
                    toAuth = new Team(user, pass);
                }
				if(user.compareTo(instructorUsername) == 0 && authenticateInstructor(pass)){
					PlannerFrame instructor = new PlannerFrame("", loginSchedule);
					LoginFrame.this.setVisible(false);
				}
				else if(loginSchedule.authenticateTeam(toAuth) == true){
					ClientFrame client = new ClientFrame("", loginSchedule, toAuth);
					LoginFrame.this.setVisible(false);
				}
				else{
					JOptionPane.showMessageDialog(null, "Incorrect Username or Password", "Invalid", JOptionPane.INFORMATION_MESSAGE);
				}
			
			}
		});
		
		this.getRootPane().setDefaultButton(loginButton);
	}
	

    public static boolean authenticateInstructor(String password) {
        if (instructorPassword.compareTo(Team.md5String(password)) == 0) {
            return true;
        }
        return false;
    }
}
