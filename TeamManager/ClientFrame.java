import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.MouseEvent;

public class ClientFrame extends JFrame {

	 private JLabel errorLabel;
	 private DefaultListModel<Meeting> model;
	 private Schedule schedule;
     private Team activeTeam;
	
     private String[] sorting = {"Start Time", "End Time", "Team"}; 
	private ArrayList<String> available = new ArrayList<String>();
	  
public ClientFrame(String path, Schedule loginSchedule, Team activeTeam) {
	
     // Start of Planner Frame ---------------------------------------------------------------------------
     super("Team Meeting Planner");
     this.setPreferredSize(new Dimension(600, 400));
     this.setResizable(false);
     this.setLayout(new BorderLayout());

     this.schedule = loginSchedule;
     this.activeTeam = activeTeam;
     available.add("Available");
     for (Team t : schedule.getTeams()) {
         available.add(t.getTeamName());
     }
     
     // Start of Left Panel ------------------------------------------------------------------------------
     JPanel leftPanel = new JPanel();
     leftPanel.setLayout(new BorderLayout());
     
     // Left Panel Components
     JPanel meetingPanel = new JPanel(new FlowLayout(5, 20, 1));
     model = new DefaultListModel<>();
     
     // load file
     schedule.parseScheduleFile(LoginFrame.scheduleFile);
     for (Meeting m : schedule.getMeetings()) {
            if (m.getTeam() != null) {
                if (!available.contains(m.getTeam().getTeamName())) {
                    m.setTeam(null);
                    m.setMeetingName("Available");
                }
            }
       model.addElement(m);
     }

     JList<Meeting> meetingList = new JList<>(model);
     JScrollPane meetingPane = new JScrollPane(meetingList);
         
     // Set Left Panel Components
     JLabel sortingHeader = new JLabel("Sort By");
     JComboBox<String> sortingList = new JComboBox<String>(sorting); 
     meetingPanel.setSize(200, 100);

     sortMeetings(sortingList);
     
     meetingPane.setMaximumSize(new Dimension(300, 400));
     meetingPane.setMinimumSize(new Dimension(300, 400));
     
     // Add Left Panel Components
     meetingPanel.add(sortingHeader);
     meetingPanel.add(sortingList);    
     leftPanel.add(meetingPane,BorderLayout.CENTER);
     leftPanel.add(meetingPanel,BorderLayout.NORTH); 
     // End of Left Panel -----------------------------------------------------------------------------------
     
     
     //Start of Right Panel ---------------------------------------------------------------------------------
     JPanel rightPanel = new JPanel();
     rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
     
     //Right Panel Components
     JPanel startTimeHeaderPanel = new JPanel(new FlowLayout(0, 0 ,0)); 
     JPanel startMeetingPanel = new JPanel(new FlowLayout(0, 0, 0));
     startMeetingPanel.setBorder(new EmptyBorder(0,0,30,0));
     JPanel endTimeHeaderPanel = new JPanel(new FlowLayout(0, 0, 0));
     JPanel endMeetingPanel = new JPanel(new FlowLayout(0, 0, 0));
     endMeetingPanel.setBorder(new EmptyBorder(0,0,30,0));
     JPanel availablePanel = new JPanel(new FlowLayout(0 ,0, 0));
     JPanel buttonPanel = new JPanel(new FlowLayout(0, 0, 0));
     buttonPanel.setBorder(new EmptyBorder(30,0,0,0));
     JPanel errorPanel = new JPanel(new FlowLayout(0, 0, 0));
     
     // Set Components for Right Panel
     JLabel startTimeHeader = new JLabel("Start Meeting Time");
     startTimeHeaderPanel.add(startTimeHeader);
      
     //Start Meeting Panel Components
     JLabel startTime = new JLabel(" ");
     
     // Add Start Meeting Panel Components
     startMeetingPanel.add(startTime);
     
     JLabel endTimeHeader = new JLabel("End Meeting Time");
     endTimeHeaderPanel.add(endTimeHeader);
     
     //End Meeting Panel Components
     JLabel endTime = new JLabel(" ");
          
     // Add End Meeting Panel Components
     endMeetingPanel.add(endTime);
     
     JLabel available = new JLabel("");
     availablePanel.add(available);
     
     JButton reserveButton = new JButton("Reserve Meeting");
     buttonPanel.add(reserveButton);
     
     errorLabel = new JLabel("");
     errorPanel.add(errorLabel);
     
     // Add Components to Right Panel
     rightPanel.add(startTimeHeaderPanel);
     rightPanel.add(startMeetingPanel);
     rightPanel.add(endTimeHeaderPanel);
     rightPanel.add(endMeetingPanel);
     rightPanel.add(availablePanel);
     rightPanel.add(buttonPanel);
     rightPanel.add(errorPanel);
     
     startTimeHeaderPanel.setVisible(false);
     startMeetingPanel.setVisible(false);
     endTimeHeaderPanel.setVisible(false);
     endMeetingPanel.setVisible(false);
     availablePanel.setVisible(false);
     buttonPanel.setVisible(false);
     errorPanel.setVisible(false);
     // End of Right Panel----------------------------------------------------------------------------------
     
     
     //Add left and right panels to Planner Frame
     JSplitPane splitPane = new JSplitPane();
     splitPane.setLeftComponent(leftPanel);
     splitPane.setRightComponent(rightPanel);
     splitPane.setEnabled(false);
     
     //Set up main Frame
     this.add(splitPane);	
     this.pack();
     this.setLocationRelativeTo(null);
     this.setVisible(true); 
     // End of Planner Frame -------------------------------------------------------------------------------
     
     
     
     // Sort Action Listener-----------------------------------------------------------------
     sortingList.addItemListener(new java.awt.event.ItemListener(){
         @Override
   	  public void itemStateChanged(java.awt.event.ItemEvent e){
           if (e.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
             sortMeetings(sortingList);
           }
         }
     });
     
     // List Action Listener-----------------------------------------------------------------
     meetingList.addMouseListener(new MouseAdapter(){

		@Override
		public void mouseClicked(MouseEvent e) {
			
			try{
				Meeting selectedMeeting = meetingList.getSelectedValue();
				startTime.setText(selectedMeeting.getStartMonth() + " " + selectedMeeting.getStartDay() + " " 
						+ selectedMeeting.getStartYear() + " at " + selectedMeeting.getStartHour() + ":" + selectedMeeting.getStartMinute() + " "
						+ selectedMeeting.getStartPeriod());
				endTime.setText(selectedMeeting.getEndMonth() + " " + selectedMeeting.getEndDay() + " " 
						+ selectedMeeting.getEndYear() + " at " + selectedMeeting.getEndHour() + ":" + selectedMeeting.getEndMinute() + " "
						+ selectedMeeting.getEndPeriod());
				
				if(selectedMeeting.getTeam() != null){
					available.setText(selectedMeeting.getTeam().getTeamName());
					
					if(Team.NameComparator.compare(selectedMeeting.getTeam(), activeTeam) == 0) {
	    				 reserveButton.setVisible(true);
	    				 reserveButton.setText("Unreserve");
	    			}
					else{
						reserveButton.setVisible(false);
					}
				}
				else{
					available.setText("Available");
					reserveButton.setText("Reserve Meeting");
					reserveButton.setVisible(true);
				}
				
				
				startTimeHeaderPanel.setVisible(true);
			    startMeetingPanel.setVisible(true);
			    endTimeHeaderPanel.setVisible(true);
			    endMeetingPanel.setVisible(true);
			    availablePanel.setVisible(true);
			    buttonPanel.setVisible(true);
			    errorPanel.setVisible(true);
				
			} catch(Exception ex){
			}
			
		}
   	  
     });
     
     
     
     //Button Action Listeners----------------------------------------------------------------
     reserveButton.addActionListener(new java.awt.event.ActionListener(){

    	 @Override
    	 public void actionPerformed(java.awt.event.ActionEvent e){

    		 if(!meetingList.isSelectionEmpty()){
    			 
    			 Meeting toReserve = meetingList.getSelectedValue();
    			 
    			 if (toReserve.getTeam() == null) {
    				 toReserve.setTeam(activeTeam);
    				 toReserve.setMeetingName(activeTeam.getTeamName());		
    				 sortMeetings(sortingList);
    			 }
    			 else if(Team.NameComparator.compare(toReserve.getTeam(), activeTeam) == 0) {
    				 toReserve.unreserve(activeTeam);
    				 sortMeetings(sortingList);
    			 }
    			 
    			 meetingList.setSelectedValue(toReserve, true);
    			 if(toReserve.getTeam() != null){
    				 available.setText(toReserve.getTeam().getTeamName());
    				 reserveButton.setText("Unreserve");
    			 }
    			 else{
    				 available.setText("Available");
    				 reserveButton.setText("Reserve Meeting");
    			 }
    		 }
    	 }
     });
     
     //Custom Close Operation
     this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
     this.addWindowListener(new java.awt.event.WindowAdapter() {
         @Override
         public void windowClosing(java.awt.event.WindowEvent windowEvent) {
             // save the new schedule to the file
              schedule.saveScheduleFile(LoginFrame.scheduleFile);
              schedule.saveTeamFile(LoginFrame.teamsFile);
			  System.exit(0);
         }
     });
     
	}

   public void sortMeetings(JComboBox<String> sortingList) {
       ArrayList<Meeting> meets = schedule.getMeetings();
       switch (sortingList.getSelectedIndex()) {
       case 0:
           Collections.sort(meets, Meeting.StartTimeComparator);
           break;
       case 1:
           Collections.sort(meets, Meeting.EndTimeComparator);
           break;
       case 2:
           Collections.sort(meets, Meeting.TeamComparator);
           break;
       case 3:
           Collections.sort(meets, Meeting.NameComparator);
           break;
       }

       model.removeAllElements();
       for (int i = 0; i < meets.size(); i++) {
           model.addElement(meets.get(i));
       }
   }

}
