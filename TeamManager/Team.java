import java.util.ArrayList;
import java.util.Comparator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Team {

    private String teamName;
    private String teamPassword;
    private ArrayList<TeamMember> teamMembers;

    public Team(String teamName) {
        this.teamName = teamName;
        this.teamPassword = "";
        this.teamMembers = new ArrayList<TeamMember>();
    }

    public Team(String teamName, String teamPassword) {
        this.teamName = teamName;
        this.teamPassword = teamPassword;
        this.teamMembers = new ArrayList<TeamMember>();
    }

    public String getTeamName() {
        return this.teamName;
    }

    public String getTeamPassword() {
        return this.teamPassword;
    }

    public ArrayList<TeamMember> getTeamMembers() {
        return this.teamMembers;
    }

    public void setTeamName(String newName) {
        this.teamName = newName;
    }

    public void setTeamPassword(String newPassword) {
        this.teamPassword = newPassword;
    }

    public void setTeamMembers(ArrayList<TeamMember> newMembers) {
        this.teamMembers = newMembers;
    }
    
    @Override
    public String toString() {
        String s = this.teamName;
        return s;
    }

    public String fileString() {
        String s = this.teamName + "|" + this.teamPassword;
        s += "|end\r\n";
        return s;
    }


    public static Comparator<Team> NameComparator = new Comparator<Team>() {
        public int compare(Team t1, Team t2) {
            if (t1 != null) {
                if (t2 != null) {
                    return t1.getTeamName().compareTo(t2.getTeamName());
                }
                return 1;
            }
            if (t2 != null) {
                return -1;
            }
            return 0;
        }
    };

    public static Comparator<Team> PasswordComparator = new Comparator<Team>() {
        public int compare(Team t1, Team t2) {
            int passCheck = t1.getTeamPassword().compareTo(t2.getTeamPassword());
            return passCheck;
        }
    };

    public static String md5String(String toHash) {
        String hashed = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] dg = md.digest(toHash.getBytes());
		    for (byte b : dg) {
			    hashed+=(String.format("%02x", b & 0xff));
		    }
        } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
        return hashed;
    }
}

